
%Now we move a step further towards our problem. We will consider in the following sequences over an alphabet $\Sigma$ composed of $|\Sigma| = \sigma $ symbols.

Our goal is to index and represent all the subsequences of a text $t$. More precisely we don't allow for a subsequence to be ``extracted'' from the whole sequence $t$, because it is generally not interesting. In other words we want to limit the region of the original sequence $t$ where we are looking for subsequences.

For example, say we are searching for a given subsequence $s$ in a text $t$. If the length of $s$ is very small in respect to the text length, $s$ is very likely to exist as subsequence of $t$ (like any other short subsequence).
It is instead more interesting to restrict the research of $s$ to all the windows of fixed length $w$ on the text, where $w$ is relatively close to $l$:

\begin{defn}Sequence $s$ is a \textit{$(l,w)$-subsequence} of $t$, if $|s| = l$ and appears in some window of length $w$ of the sequence $t$. Of course $l \leq w$.
\end{defn}


Therefore the problem can be expressed as follows. Given a text $t$, and a window size $w$ we want to build a data structure $D$ for representing \textit{all} subsequences of $t$, with length $l$ and within the window size $w$; in other words we want to index all $(l,w)$-subsequences of $t$.

Obviously we are interested to find a efficient solution to this problem. One in fact could think to build $m = |t| - w + 1$ indexes for the $m$ different window positions and then merge the results (in the following we refer these indexes as $D_0, D_1, \cdots, D_m$), but in this case we are considering many times subsequences that we have already considered.

For example the dictionary $D_0$ contains the $(l,w)$-subsequences inside the first window position, starting from $t[0]$ and ending in $t[w-1]$. Now, moving the window of one position, consider the same set $D_1$. It's evident that there are \textit{many} common sequences between $D_0$ and $D_1$ and it is not convenient to rebuild them all.
%In particular all the $(l,w)$-sequences in $D_0$ that comes from the window $t[1,w-1]$ (that is all sequences that do not contain the first character), are certainly contained also in $D_1$, since these items $t[1, w-1]$ are a subset of $t[1, w]$ that is the window for $D_1$.

\section{Analysis of the basic subproblem}
We first start analysing the problem of finding all the subsequences of a text $t$ of length $w$. We forget for a moment the moving window, we just restrict the original problem to a single window position. This is already difficult because of the huge number of subsequences that are contained also in a text of modest length.

An upper bound for the number of these sequences is easy to compute if we consider the case where the symbols in the string $t$ are all different, therefore $\sigma \geq w$.\footnote{This case is fictitious for the vast majority of the cases, where in fact $\sigma < w$.}. In this case the number of $(l,w)$-sequences is simply ${w \choose l}$ that is the number of ways of choosing $l$ elements in a set of $w$.

When there are repeated symbols in $t$, or the alphabet is smaller than $w$ the number ${w \choose l}$ is still a valid upper bound but clearly it's not reachable by any configuration of symbols. Another weakness of using ${w \choose l}$ is that it does not depend on $\sigma$, while the total number of subsequences is evidently depending also on the alphabet size, as explained below. %And there are cases where for example if $t$ contains only one symbol, clearly there is only 1 $l$-sequence for any $l$, instead of the huge ${w \choose l}$. 
Furthermore, to complicate the problem there is the fact that given a string, the number of possible $(l,w)$-subsequences depends on the exact configuration of its symbols, not only on the numbers $l$, $w$ and $\sigma$.% I think there is no exact formula for this.

What we can do instead is to observe the behaviour on average, of the number of $(l,w)$-sequences in a random sequence of length $w$ from an alphabet $\Sigma$. We call this average number $u(l,w,\sigma)$. This function is shown in figure \ref{fig:seqnumber}, with $w = 16$, various alphabet sizes and $l$ ranging from 1 to $w$. Clearly smaller the alphabet, lower is the average number of subsequences, because there are more repetitions and less entropy. At the other end we have instead:
\begin{align}
\lim_{\sigma \rightarrow \infty} u(l,w,\sigma) = {w \choose l}
\end{align}
That means: larger is the alphabet, higher is the possibility to have all different symbols in the window, reaching the upper bound.

\begin{figure}[h]
\center
\begin{tikzpicture}
\begin{semilogyaxis}[
	xlabel=$l$,
	ylabel=number of subsequences,
	height=0.45\textheight,
	width=\textwidth,
	xmin=0,
	xmax=17,
	ymax=500000,
	legend pos=north east,
	%patch type=quadratic spline,
]
		
	\addplot[color=red, only marks, mark=x]   table[x index=0,y index=1] {plot-data/plot-16-4};
	\addlegendentry{$|\Sigma| = 4$, $w = 16$}
	\addplot[color=green, only marks, mark=square]   table[x index=0,y index=1] {plot-data//plot-16-8};
	\addlegendentry{$|\Sigma| = 8$, $w = 16$}
	\addplot[color=blue, only marks, mark=o]   table[x index=0,y index=1] {plot-data//plot-16-16};
	\addlegendentry{$|\Sigma| = 16$, $w = 16$}
	\addplot[only marks, mark=star]   table[x index=0,y index=2] {plot-data//plot-16-4};
	\addlegendentry{binomial bound ${16 \choose l}$}
%	\addplot[color=red]   table[x index=0,y expr=2000*(2.71828182846)^(-0.1135*(-8.5+x)^2)] {code/plot-16-4};
%	\addlegendentry{$ $}
\end{semilogyaxis}
\end{tikzpicture}
\caption[]{The average number of $(l,w)$-sequences in random strings of length $w = 16$, from different alphabet sizes. Notice that, also if the trend is the same for all the curves, between the red plot and the bound there is a factor of $10^3$ (in the central part). For every point in the plot 10000 random strings were drawn.}
\label{fig:seqnumber}
\end{figure}

%\begin{tikzpicture}
%\begin{semilogyaxis}[
%	xlabel=$l$,
%	ylabel=number of subsequences,
%	height=0.5\textheight,
%	width=\textwidth,
%	xmin=1,
%	xmax=32,
%	%ymin=0,
%	legend pos=south west,
%	patch type=quadratic spline,
%]
%		
%	\addplot[color=red,smooth]   table[x index=0,y index=1] {code/plot};
%	\addlegendentry{$|\Sigma| = 4$, $w = 32$}
%	\addplot[smooth]   table[x index=0,y index=2] {code/plot};
%	\addlegendentry{binomial bound ${32 \choose l}$}
%\end{semilogyaxis}
%\end{tikzpicture}

Algorithm \ref{alg:seq} implement the na\"{\i}ve approach for solving this subproblem. The output is a data structure, an index, containing all $l$-sequences on the input string $t$. The algorithm starts inserting in $D$, the dictionary that will be returned, the prefix of length $l$ of $t$. Then it begins to consider the remaining symbols, one at the time, generating from the sequences in $D$ all the $(l-1)$-sequences, in order to insert the new symbol (lines \ref{alg:seqloopbegin}-\ref{alg:seqloopend}). These new sequences are then inserted in $D$ (line \ref{alg:seqinsert}).

\begin{algorithm}[h]
\SetAlgoLined
\DontPrintSemicolon
\KwData{a string or sequence $t$, with $|t| = w$, an integer $l < w$}
\KwResult{an index $D$}
\BlankLine
$D \leftarrow \{ t[1, \ldots, l]\}$\;
\For{$i \leftarrow l+1$ \KwTo $|t|$}{
	$E \leftarrow \emptyset$\; \nllabel{alg:seqloopbegin}
	\For{$d \in D$}{
		$F \leftarrow SEQ( d, l-1 )$\; \nllabel{alg:seqrecursion}
		$E \leftarrow E \cup ( F \sqcup S[i] )$\;
	} \nllabel{alg:seqloopend}
	$D \leftarrow D \cup E$\; \nllabel{alg:seqinsert}
}
\KwRet{$D$}
\caption{$SEQ$}%, that builds an index of all subsequences of length $l$ of a text of size $w$.}
\label{alg:seq}
\end{algorithm}

The time complexity of this simple procedure is high, and not as straightforward to calculate as one may think for an algorithm that simple. In the following we will assume that the insertion in a dictionary $D$ costs $c$, not dependent on the size of the dictionary. The union o two dictionaries $D$ and $E$ costs $c$ times the size of the smaller of the two, e.g. $c \cdot \min( |D|, |E| )$. This is a quite general setting and holds, at least on the \textit{average} analysis, for example for structures like hash tables and tries. More in general $c$ is a function of the size of the data structure where insertion is taking place.

Under these settings the time complexity $f(l,w,\sigma)$ of the procedure \ref{alg:seq} can be computed as follows. The outer loop consists of exactly $w-l$ steps because runs on the remaining part of $t$. The inner loop runs on all the current elements of $D$, number that changes with the iteration index $i$; name this number $|D|_i$. Inside this loop we have a recursive call that costs $f(l-1,l,\sigma)$. Actually this recursive call can be solved in $\Theta ( c\cdot l)$ since we are generating the sequences from $d$ that lack of one character. All these sequences have then to be inserted into $E$ for a cost of $c$ times their number. Putting all together:
\begin{align}
f(l,w,\sigma) &= c + \sum_{i=l+1}^w (cl + c)|D|_i + c |D|_i \\
&= c + (cl + 2c) \sum_{i=l+1}^w |D|_i \\
&\simeq cl \sum_{i=l+1}^w c |D|_i \\
\end{align}

This recursive expression can be simplified considering that the number $|D|_i$ can be upper bounded by $u(l,i,\sigma)$, since we are taking $(l,i)$-sequences from a window of size $i$ that is growing in the text. So we finally end up with:
\begin{align}\label{eqn:bound}
f(l,w,\sigma) &\simeq cl \, \sum_{i=l+1}^w u(l,i,\sigma)
\end{align}

Using the upper bound ${i \choose l}$ for $u(l,i,\sigma)$ we can further simplify the above expression using the Pascal's rule for binomial coefficients (and natural induction) considering that:
\begin{align}
\sum_{i=l}^w {i \choose l} = {w+1 \choose l+1}
\end{align}

Therefore equation \ref{eqn:bound} become:
\begin{align}
f(l,w,\sigma) &\simeq c l \, {w+1 \choose l+1} \simeq c w \, {w \choose l}
\end{align}

Another approach is to generate all the combinations $w \choose l$, and efficient algorithms exist for this purpose \cite{knuth4.2}, requiring only $O(l)$ additional computation for every combination, that correspond to its generation.
The drawback of this approach is that in this case we always pay exactly $l{w \choose l}$, also if the input characters configuration is trivial, e.g. many repetitions of the same symbol.

\section{Analysis of the whole problem}
Now, assuming that the whole text has length $n$, we consider the additional step of moving the window through the $n - w + 1$ possible positions. If we have $n$ different characters in the text again the solution is easy. We have to count for the $w \choose l$ sequences in the first window position, as explained in the previous section and exactly $w-1 \choose l-1$ for any other position.
The explanation is simple. Assuming we solved the first window position, we advance of one symbol. How to build the new sequences? Take $l-1$ characters from the first $w-1$ symbols and then add the new one. Therefore we have exactly $w-1 \choose l-1$ new sequences, and this is the same number for every additional window position. At the end we have a total of
\begin{align}\label{upper2}
{w \choose l} + (n - w){w-1 \choose l-1} = \left( 1+\frac{l(n - w)}{w} \right){w \choose l}
\end{align}
subsequences. This means that if the text has length $n$, the total number of subsequences is around $n$ times the number of sequences in a single window position.

While before considering all different symbols was a valid upper bound, here the situation becomes quite different. Having a finite alphabet, where usually $\sigma \ll n$, clearly sets a limit on the number of total subsequences $U(l,w,\sigma,n)$, even if the total length of the text grows infinitely.
At some point in fact we eventually reach the total number of sequences of length $l$ that can be built with $\sigma$ symbols, that are exactly $\sigma ^ l$. So in this case we can write:
\begin{align}\label{upper1}
\lim_{n \rightarrow \infty} U(l,w,\sigma,n) = \sigma^l
\end{align}
While fixing $n$ and studing $U(l,w,\sigma,n)$ in function of $\sigma$ we have, as explained before:
\begin{align}
\lim_{\sigma \rightarrow \infty} U(l,w,\sigma,n) = \left( 1+\frac{l(n - w)}{w} \right){w \choose l}
\end{align}
similarly to the single window scenario, larger the alphabet, more likely to have all different symbols in the text.

\section{Motivation}
The numbers just introduced are huge. Even if the numbers we are playing with are small, the bounds of equations \ref{upper1} and \ref{upper2} grows extremely quickly. What we can do is to design good algorithms in terms of output sensitive complexity. For example algorithm \ref{alg:seq} has a polynomial complexity in terms of the output size.

The choice of BDDs came in this optic. BDDs are used to compactly represents sets of objects, making possible to work directly on the encoded representation. If this representation is very small compared to the real objects, it's possible to handle problems that would be otherwise intractable.

So our goal is to see if BDDs can be a good choice in storing sets of sequences, trying to express our algorithms in terms of sets operations, to see if we can take advantage of the BDDs algebra.



\section{A possible methodology}
If algorithm \ref{alg:seq} can be the basic building block for solving the problem we still need to handle efficiently the sliding window.
%The previous example holds in general for any pair of consecutive window positions in $t$, and lead to a possible optimization of the whole dictionary construction. 
It's useful to look at the original string $t$ as $\boldsymbol{\alpha} x \boldsymbol{\beta} y$, where $\boldsymbol{\alpha}$ is an arbitrary prefix of $t$, $\boldsymbol{\beta}$ is a string of $w-1$ symbols and $x,y$ are single symbols.

Suppose we built somehow the partial index $D^k = D_0 \cup D_1 \cup \cdots \cup D_k$ containing all the $(l,w)$-subsequences of $t$ before character $y$, that is until the window was including $x \boldsymbol{\beta}$. In this context we call $D_i$ the set of sequences corresponding to the window position $i$, also if what we are trying to do is to not comput it explicitly. We are going in fact to analyze which are the common subsequences in two consecutive window positions.

Now, moving the window to $\boldsymbol{\beta} y$, we can characterize all the new sequences that will be added, or in particular the index for the window $\boldsymbol{\beta} y$ that we indicate with $D_{k+1}$. 
%There are two possibilities:
%\begin{enumerate}
%\item \label{it:subx} $s$ is one of the $(l,w-1)$-subsequences in $x \boldsymbol{\beta}$ that do not contain $x$ as first item.\footnote{They are $(l,w-1)$ and not $(l,w)$-subsequences for this fact.} Therefore these are directly $l$-subsequences of $\boldsymbol{\beta}$.
%\item \label{it:suby} $s$ is built from a $(l,w)$-subsequence that has $x$ as first symbol. In particular we add the new symbol $y$ to the end and we remove the starting $x$ symbol.
%\end{enumerate}

%At this point it's important to notice that the sequences falling in the first case are the common sequences between $D_k$ and $D_{k+1}$ (they do not contain neither $x$ or $y$). And it should be clear, but we can easily prove the following.

\begin{thm}\label{thm:sequences}
Given the index $D_k$ of the $(l,w)$-sequences in $x \boldsymbol \beta$, we have two possibilities for all the $(l,w)$-sequences $s$ of the new index $D_{k+1}$ for $\boldsymbol{\beta} y$:
\begin{enumerate}
\item \label{it:subx} $s$ is one of the $(l,w)$-sequences of $\boldsymbol{\beta}$. %that do not contain $x$ as first item. %Therefore these are directly $l$-subsequences of $\boldsymbol{\beta}$.
\item \label{it:suby} $s$ is a $(l-1,w-1)$-sequence of $\boldsymbol \beta$ where $y$ has been added at the end.
\end{enumerate}
All these sequences can be constructed from the index $D_k$.
\end{thm}
\begin{proof}
One subsequence $s$ of $\boldsymbol{\beta} y$ may have or not the new character $y$. If it has not, then we fall in the case \ref{it:subx}, and these sequences are clearly in $D_k$.%, because $s$ is a $(l,w-1)$-subsequence of $\boldsymbol{\beta}$.

Otherwise $s$ is a subsequence of the form $\gamma y$, where $\gamma$ can be any $(l-1,w-1)$-subsequence of $\boldsymbol{\beta}$. Then, sequence $x \gamma$, can be any of the $(l,w)$-subsequences of $x \boldsymbol{\beta}$ that, having $x$ as first symbol, fall into the case \ref{it:suby} and is included as well in $D_k$.
\end{proof}


These considerations lead to a possible method for building the index $D$ efficiently. Every time we move the window of one position, we don't need to care about the sequences of point \ref{it:subx}, because they are already in the index. Instead we have to build quickly all the sequences of point \ref{it:suby}, all the $(l-1, w-1)$-sequences $\gamma$ where $y$ will be added. 

One possibility can be to request from $D^k = D_0 \cup \cdots \cup D_k $ all $(l,w)$-subsequences that starts with a $x$ and filter the ones that effectively are subsequences of $x \boldsymbol{\beta}$. In this way we can build all the (potentially) new subsequences as we stated before, deleting the leading $x$ and adding the new symbol $y$.
This can still be inefficient, though. Because if from one side the filtering can be done in just $O(w)$ (testing for the sequence to appear in $x \boldsymbol{\beta}$), the number of such candidate sequences may be huge: they are all the sequences starting with an arbitrary symbol of $\Sigma$.

One promising idea could be to keep track, every time we move the window of these interesting sequences that we will need at the next step. In particular we store at least a part of the sequences of $D_k$, the ones that start with $x$, that is those sequences satisfying point \ref{it:suby}. The number of these sequences can be big as well but they are the only ones we need to generate all the (potentially) new subsequences to add to the index.

The following sections discuss this second approach.





\subsection{A further step}
%Now is time to discuss the central part of the algorithm, taking into account the sliding window. 
The pseudocode for this part is represented in algorithm \ref{algostep} and figure \ref{fig:movingwindow}. The basic idea is to compute, for every window position $i$ the current index $E$ and the index $D_i$, given the two previous indexes $D$ and $D_{i-1}$.
The first one represent the whole index, the one that we want to fill for solving the original problem. The second one is a smaller index containing all the $(l,w)$-sequences for the window at position $i$ (that is $t[i,i+w]$).

\begin{algorithm}[b]
\SetAlgoLined
\DontPrintSemicolon
\KwData{a string or sequence $t$, integers $l, w, i$ with $l < w$ and $i < |t|$, two indexes $D$ and $D_{i-1}$}
\KwResult{indexes $E$ and $D_i$}
\BlankLine
$x \leftarrow t[i-w]$\;
$y \leftarrow t[i]$\;
$D_i^{x} \leftarrow \{ d \in D_{i-1}\, |\, d$ begins with $x \}$\; \nllabel{stepinit1}
$D_i^{\overline{x}} \leftarrow D_{i-1} \setminus D_i^{x}$\; \nllabel{stepinit2}
%\For{$d \in D_{i-1}$}{}
\BlankLine
$D_i^{x} \leftarrow (D_i^{x} \sqcap x) \sqcup y$\; \nllabel{stepmodify}
$D_i \leftarrow D_i^{x} \cup D_i^{\overline{x}} $\;
$E \leftarrow D_i^{x} \cup D $\; \nllabel{stepunion}
\KwRet{$E, D_i$}
\caption{$STEP$}
\label{algostep}
\end{algorithm}

\input{figures/window}

The analysis of this procedure is quite simple. The first lines \ref{stepinit1} and \ref{stepinit2} are linear in the size of $D_{i-1}$, in particular we have a cost $c \cdot | D_{i-1} |$ because of the insertions in the new dictionaries $D_i^x$ and $D_i^{\overline x}$. The cost of insertions and deletions on the strings in line \ref{stepmodify} can be assumed to be $O(1)$, so we pay only the insertions in the dictionary $D_i^x$. We can upper bound the last two lines with $c\cdot |D_i^x|$ since in line \ref{stepunion} it is the smaller of the two (like in the above line). Therefore the total complexity $g(l,w,\sigma)$ is:
\begin{align}
g(l,w,\sigma) \simeq c\, ( | D_{i-1} | + | D_i^x | )
\end{align}

Now we notice that the size of $D_i^x$ is on average $u(l-1,w-1,\sigma)$ because on line \ref{stepmodify} we are retrieving the $(l-1,w-1)$-sequences of $\boldsymbol \beta$ in theorem \ref{thm:sequences}, adding a trailing $y$. And of course we assume as before $D_{i-1} \simeq D_i \simeq u(l,w,\sigma)$. Therefore the previous equation become:
\begin{align}
g(l,w,\sigma) \simeq c \, ( u(l,w,\sigma) + u(l-1,w-1,\sigma) )
\end{align}


\subsection{Putting all together}
The two previous sections introduced two algorithms $SEQ$ and $STEP$ that are used to solve the original problem with algorithm \ref{alg:all}. It really a straightforward procedure that simply initialize $D$ using algorithm $SEQ$ and then call $STEP$ for every other window of the string $t$. If the total lenght of the string $t$ is $n$, the algorithm runs in time $h(l,w,\sigma)$ simply computed as:
\begin{align}
h(l,w,\sigma) &= f(l,w,\sigma) + (n-w)\cdot g(l,w,\sigma)
\end{align}

\begin{algorithm}[h]
\SetAlgoLined
\DontPrintSemicolon
\KwData{a string or sequence $t$, integers $l, w$ with $l < w$}
\KwResult{and index $D$}
\BlankLine
$D \leftarrow SEQ(t[1,w], l)$\;
$E \leftarrow D$\;
$n \leftarrow |t|$\;
\BlankLine
\For{$i \leftarrow w+1 $  \KwTo $n$ }{
	$\langle D, E \rangle \leftarrow STEP( t, l,w,i,D,E )$\;
}
\BlankLine
\KwRet{$D$}
\caption{$ALL$}
\label{alg:all}
\end{algorithm}
