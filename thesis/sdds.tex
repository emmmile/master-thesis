\label{cha:sdds}
%\section{An alternative definition}
In this chapter we define what is a sequence binary decision diagram starting directly from ZDDs. Under this definition SDDs are nothing more than ZDDs, having just a substantial difference. Since the goal is to represent sequences, and in particular strings, in every node, rather than storing the index of a variable, is stored directly the symbol $\alpha \in \Sigma$ that this variable is referring to. 

%Semantically it represents a boolean function that is ``tuned'' to efficiently represent a sequence of objects. If not specified it will be assumed that 
\begin{defn}[structure]
A sequence binary decision diagram $S$ is a zero suppressed decision diagram, that is a representation of boolean function $f(x_1$, $\ldots$, $x_n)$. In particular it respects the ordering and reduction properties of definition \ref{zdd:definition}. 
\end{defn}

Also if SDDs in theory could be used for handling generic sequences, here it is not used any universe of objects $U$, while it is preferred an alphabet $\Sigma$, where the symbols can be directly stored in the nodes. Doing this we are not loosing in generality, since the general case can always be mapped to a finite alphabet.

Nevertheless as in ZDDs, there exist a variable ordering $\prec$ such that, for any branch from node $v$ to node $w$, the relative variables $x_v$ and $x_w$ satisfy $x_v \prec x_w$ (notice that we are not referring to the symbols stored in the nodes). This ordering will be studied in the following. Of course any \textit{symbol} can occur many times in the same sequence, but this fact will be encoded with \textit{different variables}, that in every case, satisfy what has been just stated. In other words a symbol is allowed to appear many times, while a variable is not (as usual). Consider as additional example an SDD containing only words with one symbol. Clearly this diagram, seen as a boolean function, is not a function of one variable, but every node represents a symbol in a specific position (therefore a different variable), similarly to ZDDs.

In fact an SDD represents a family of sequences with an encoding that is an extension of the one presented in \ref{def:naivesequences}.
%For this reason, to avoid confusion, this notation it is not used anymore, changing it a bit in the following way.

\begin{defn}[sequence encoding]
Let $\Sigma = \{ \alpha_1, \alpha_2, \ldots, \alpha_n \}$ be an alphabet. A set of sequences on $\Sigma$ can be encoded by a boolean function $f(x)$, where each variable $x_{\alpha_j}^I$ represents the presence or not of $\alpha_j$ in a certain set of positions $I$ of the sequence. Which position $i \in I$ the variable is actually referring to depends both on the variable ordering that has been chosen, and on the values of the variables smaller than $x_{\alpha_j}^I$.
\end{defn}

In the following we call the set $I$ the set of \textit{contexts} for variable $x_{\alpha_j}^I$. Also, instead of writing $x_{\alpha_j}^I$, the notation $\alpha_j^I$ will be preferred for simplicity. From the previous definition, for defining the contexts of a variable we need to decide a direction of traversing the sequences that are represented, and usually a left to right approach is used.

For example, say we want to represent the family of sequences $F = \{abc$, $ab$, $bc$, $b \}$ on the alphabet $\Sigma = \{ a, b, c \}$. To decide or not the membership of any string to $F$ it is possible to just use three variables $a^{\{1\}}, b^{\{1,2\}}, c^{\{2,3\}}$ as shown in the diagram \ref{fig:sddexample}. Consider for example the variable $ b^{\{1,2\}}$: if an $a$ appear in first position, then the occurrence of $b$ must be tested on the next, position two. Otherwise if no $a$ is found in first position the occurrence of $b$ must be tested again on the first position $1$.
So for example the string $bc$ is recognized with the configuration of variables $a^{\{1\}} = false$ and $b^{\{1,2\}} = c^{\{2,3\}} = true$, that evaluates to \sinkh{} in the diagram.

At this point, a possible well defined order $\prec_V$ that can be used for the purpose of SDDs is the following.
\begin{defn}[variable order]\label{sdd:ordering}
For any pair of variables $\alpha_j^J$ and $\alpha_k^K$ follows that $\alpha_j^J \prec_V \alpha_k^K$ if and only if $J$ is lexicographically smaller than $K$. For variables with equal set of contexts any symbol ordering can be used to define $\prec_V$.
\end{defn}
\margin{Explain what means lexicographically smaller for sets?}

With lexicographically smaller here we mean the ordering of the sorted representation of the set, from the bigger context to the smaller one. For example set $\{ 2, 4, 7 \}$ is greater than $\{ 6, 3, 4, 1 \}$ because their ordered representation is $\{ 7, 4, 2 \}$ and $\{ 6, 4, 3, 1 \}$.

A few words have to be spent concerning the variables with equal set of contexts. A variable ordering does not imply an ordering over the symbols of $\Sigma$, while an ordering over the symbols can be used to set an order between the variables with a fixed set of contexts. For example, considering $\Sigma = \{ a, b\}$, from the variable ordering:
\begin{align}
a^{\{ 1 \}} \prec b^{\{ 1 \}} \prec b^{\{ 2 \}} \prec a^{\{ 2 \}} 
\end{align}
\noindent no meaningful order on $\Sigma$ derives. But using the order $a \prec_\Sigma b$ it is possible to build a different variable ordering:
\begin{align}
a^{\{ 1 \}} \prec b^{\{ 1 \}} \prec a^{\{ 2 \}} \prec b^{\{ 2 \}} 
\end{align}
\noindent that still is a well defined ordering in the sense of definition \ref{sdd:ordering}, and basically is the same that the authors of \cite{loekito} unconsciously used. And of course an ordering respecting also $\prec_\Sigma$ is something more practical and easy in a possible implementation.

\input{figures/sddexample}

Defining an ordering on the contexts is important to give an ordering on the variables, but an additional property has to be guaranteed. Paths made of LO edges paths need some kind of restriction in order to avoid redundant informations, or even worse incongruence.
\begin{defn}\label{sdd:lopaths}
In a SDD any path made of LO edges never contains the same symbol twice.
\end{defn}

This is necessary because two variables with different contexts (therefore different variables) may be connected together by a LO edge; without any restriction these variables may refer to the same symbol. And this, from the semantical point of view could create a contradiction.

Given an SDD and a possible variable ordering (left to right if not specified), it is possible to compute the contexts of each variable, simply traversing the SDD top-down, as shown by figure \ref{fig:sddexamplevariables}. Also if the symbols are not shown (left part of the figure) no ambiguity arise on which positions in the sequences each branch is referring to. This can be done following these simple rules:
\begin{enumerate}[i)]
\item The context of the root is simply $\{1\}$. %This corresponds to evaluating the sequence left to right, therefore the first position that we encounter is tested at the root.
\item For any branch node $v$, let $L_v$ be the union of the contexts of all LO-parents, and $H_v$ the union of the contexts of all HI-parents. Its possible we compute the set $I_v$ as:
\begin{align}
I_v = L_v \cup \{ i + 1 \, | \, i \in H_v \}
\end{align}
\end{enumerate}

The above rules are quite natural to understand, and represent the fact that, traversing an HI edge we proceed into the next position of the sequence, while after traversing a LO edge we are going to test an alternative symbol on the same position. Now is possible to show that the ordering \ref{sdd:ordering} is well defined.

\begin{thm}\label{sddorderthm}
Let $S$ be an SDD and $v_i$, for $i = 0, 1, \ldots, n$, the variables representing its branches. For any two pair of different variables $v_i$ and $v_j$ with $v_i \prec v_j$, no edge links a branch on $v_j$ with a branch on $v_i$.
\end{thm}
\begin{proof}
%Let $v_i = \alpha^I$ and $v_j = \beta^J$ (the symbol $\alpha$ can be equal to $\beta$ only if $I \neq J$).
This proof is by contradiction. Let $x \in S$ be a node branching on $v_i$ and $y \in S$ a node branching on $v_j$. Suppose that an edge from $y$ to $x$ exist in the diagram. Let the contexts of these nodes be $I = \{ i_1, i_2, \ldots, i_m \}$ and $J = \{ j_1, j_2, \ldots, j_l \}$.

If there is an HI edge from $y$ to $x$, it has to be $i_m = j_l + 1$, but this contradict the fact that $v_i \prec v_j$. If a LO edge exists instead, also in this case it would not be possible to have $I < J$ because $J$ would be contained in $I$. Therefore no edge from $y$ to $x$ can exist.
\end{proof}

Having a well defined variable order like in ZDDs, together with property \ref{sdd:lopaths}, avoid redundancy in every path.
\begin{lem}
Any top-down in path in a SDD never evaluates the same variable twice.
\end{lem}

\input{figures/comparison}


At this point should be clear the key difference between ZDDs and SDDs. In ZDDs the information on which variable each branch refers is encoded with an index (the variable index) that is stored in each branch node. In SDDs the same information is conceptually encoded both with the symbol that is stored in the node, and with the structure of the diagram (in particular the structure of the paths going from the root to that node).
Part of the encoding of the problem, that before was totally a separate aspect from the diagram (section \ref{sec:symbolic}), now is partially encoded in the diagram itself. So in this sense SDDs are really a more compressed representation.

Despite this difference it is possible to use the same notion of isomorphism we used in chapter \ref{cha:bdds}, also if the two nodes $v$ and $w$ have different contexts: two isomorphic nodes with different contexts are still merged together in a reduced diagram. In fact the set of the contexts for node $v$ is an information that depends on the parents of $v$, while being isomorphic or not with another node depends only on the childs and the label. This consideration, together with the previous ones lead to the formal definition of SDD in the style of BDD, simply modifying to some extent the ordering and the reduction rules.

\begin{defn}\label{sdd:definition}
A sequence BDD $S = \langle V, E \rangle$ is a directed acyclic graph with nodes and edges described as for BDDs and ZDDs, respecting the properties discussed in this section and following the rules:
\begin{description}
\item[Order] a well defined ordering of variables $\prec_V$ exists in order to satisfy $v \prec_V w$, for any edge going from vertex $v$ to vertex $w$, with $v, w \in V$.\label{sddorder}
\item[Reduction] no branch node has HI edge equal to \sinkl{}. Additionally there is no vertex $v$ in the graph that is isomorphic to a distinct vertex $w$.\label{sddreduction}
\end{description}
\end{defn}

\section{Reduction algorithm}

What is interesting is that following this approach is possible to define a reduction algorithm, similar to the one presented for BDDs and ZDDs. It takes as input a ZDD with the encoding \ref{def:naivesequences} or in general a non reduced SDD and return the minimal diagram.

The key point in this algorithm, and the main only difference with the one presented for BDDs, is to find a node ordering compatible with property \ref{bddreductionorder}, that guarantees a traversal of the vertices in such a way that every new node only points to already reduced nodes. This ordering is different from the one presented for BDDs as explained in the latter.

The example of figure \ref{fig:comparisonnonreducedsdd} can be stored in the following triples, for example:\begin{equation}
\begin{split}
0 &\rightarrow \langle \infty, 0, 0 \rangle \\
1 &\rightarrow \langle \infty, 1, 1 \rangle
\end{split}
\qquad
\begin{split}
2 &\rightarrow \langle c, 1, 1 \rangle \\
3 &\rightarrow \langle a, 2, 1 \rangle
\end{split}
\qquad
\begin{split}
4 &\rightarrow \langle b, 0, 3 \rangle \\
5 &\rightarrow \langle a, 4, 4 \rangle
\end{split}
\end{equation}

\noindent and the root is node 5. Sorting these triples in decreasing order on the symbol, then in lexicographic increasing order on the pointers, produce the sequence of nodes $0$, $1$, $2$, $4$, $3$, $5$. This means that following this order node $4$ will be considered before nodes $3$ and $5$. This has obviously no sense from the reduction point of view, because we would like to have a bottom-up behaviour, but this ordering is merely based on the symbol. This behaviour was achieved in the ZDD case thanks to the ordering property of the variables indices. Here we do not have this guarantee since there are symbols instead.


\begin{algorithm}[p]
\SetAlgoLined
%\DontPrintSemicolon
\KwIn{A non reduced SDD $A$ with $N$ nodes}
\KwOut{A reduced SDD $B$ equivalent to $A$}
\SetKw{Continue}{continue}
\BlankLine
compute the distances $d[i]$, for $i = 0,\ldots, N-1$\;
let $C$ be the array $\langle A[i], i, d[i] \rangle$, for $i = 0,\ldots, N-1$\;
let $reduced[i] \leftarrow i$, for $i = 0,\ldots, N-1$\;
sort $C$ in increasing order based on the distance\;
$last \leftarrow -1$\;
%write the sinks in the output $B$, and $last \leftarrow 1$\;
\For{$d\leftarrow 0$ \KwTo $d_{max}$}{
	let $C[i,j)$ the range with distance equal to $d$\;
	translate every element in $C[i,j)$ according to $reduced$\; \label{sddtranslation}
	sort $C[i,j)$ in lexicographic increasing order\;
	\For{$k\leftarrow i$ \KwTo $j-1$}{
		let $\langle S, x \rangle \leftarrow C[k]$\; 
		\uIf{$S$ has $HI = 0$}{
			$reduced[x] \leftarrow $ $LO$ pointer of $S$\;
			\Continue\;
		}
		\ElseIf{$S \neq B[last]$}{
			$last \leftarrow last + 1$\;
			$B[last] \leftarrow S$\;
		}
		$reduced[x] \leftarrow last$\;
	}
}
\caption{SDD reduction}
\label{alg:sddreduction}
\end{algorithm}

Here the same condition is achieved introducing a quantity associated to each node, that will be easily computed.

\begin{defn}[distance]
For any node $i$ in the SDD, the maximum distance from $i$ to a sink, or simply the distance of $i$, is the maximum length of any path from node $i$ to \sinkl{} or \sinkh{}.
\end{defn}

\margin{Put the algorithm? It is really simple...}
To compute the distance for every node in the diagram suffices to make a simple breadth first visit, filling an array $d[i]$ that contains one entry for every node. The complexity of this visit is linear in the number of nodes, assuming that we are properly marking each visited node (the array $d$ itself can be used for this purpose).
One may ask why for computing $d$ we consider also the distance to \sinkl{}, and not only the paths to \sinkh{}, that are the ones that actually express the sequences. The answer is simple: both paths are needed in order to define an isomorphism notion. In fact two nodes $v$ and $w$ are isomorphic if all their subgraphs are equivalent, not only the one reachable trough the HI pointer. And in particular if two subgraphs are equivalent they have the same path lengths.

The algorithm \ref{sddreduction} shows how the new ordering is computed. The whole diagram is firstly sorted according to the distance. Then for every distance bucket, line \ref{sddtranslation} redirects to the correct (new) positions. Then a second sort is made on these pointers in such a way that the following reduction can be made as in the ZDD case. In fact the following property holds.

\begin{thm}
A node at distance $i+1$ only points to nodes that have distances in the range $0, 1, \ldots, i$.
\end{thm}
\begin{proof}
This can be easily proved using induction. The nodes at distance 0 are the sinks and they points only to their selves. And assuming the hypothesis for step $i$, there is no possibility that a node at distance $i+1$ points to a node with distance $i+1+k$ with $k>0$, since otherwise it would not be part of step $i+1$ but of step $i+1+k$. 
\end{proof}

The algorithm is also correct since every pair of isomorphic nodes in the input (that has therefore to be reduced) appear within the nodes with the same distance, otherwise the pair would not be isomorphic.

This algorithm, as the previous one, has worst case complexity $O(N)$, assuming the use of linear sorting routines, and can be used also for BDDs or ZDDs (not necessarly storing sequences).

\section{Multi-way reduction algorithm}\label{sec:partial}

The presented reduction algorithm may serve as principal ingredient to a multi-way reduction algorithm, that will be presented here more in depth.

The purpose of this algorithm is to create a compact representation for a set of diagrams, sharing all the common nodes like in a BDD base. This approach actually creates an SDD base, avoiding the use of hash tables.
%But there are already many efficient data structures for handling suffixes, like for example suffix trees and suffix arrays.

\margin{Example of compression rate?}
This algorithm takes as input $M$ diagrams, and returns an SDD base where all these diagrams share nodes. Initially the nodes of the input SDDs are written into a big array, storing the pointers of the roots in a separate space. All these diagrams initially share only the sinks, and this is crucial for the successive steps. After processing all the strings, a single pass reduction is applied in order to merge all the isomorphic nodes. All the root pointers can be translated afterwards using the $reduced$ function of algorithm \ref{alg:sddreduction}.

\begin{algorithm}[h]
\SetAlgoLined
%\DontPrintSemicolon
\KwIn{A set $S$ of SDDs}
\KwOut{An SDD base containing the diagrams in $S$, composed by an array of nodes $V$ and an array of pointers $roots$}
\SetKw{KwInn}{in}
\BlankLine
$last \leftarrow 0$\;
\For{$T$ \KwInn $S$}{
	%\For{$\langle \alpha, l, h \rangle$ \KwInn $T$}{
	\For{$i\leftarrow 0$ \KwTo $|T|$}{
		let $X \leftarrow \langle \alpha, l, h \rangle \leftarrow T[i]$\;
		\uIf{$\alpha \neq \infty$}{
			$X \leftarrow \langle \alpha, l + last, h + last \rangle$\;
		}
		$V[last + i] \leftarrow X$\;
	}
	$last \leftarrow last +|T|$\;
}
reduce $V[0, last]$ with algorithm \ref{alg:sddreduction}\;
reroute the pointers in $roots$ according to $reduced$, as in algorithm \ref{alg:sddreduction}\;
\caption{SDD base construction}
\label{alg:sddbaseconstruction}
\end{algorithm}

Given an input $N$ total nodes, the present algorithm has worst case complexity $O(N)$ since, for every node in the input, a node is created in the output. On these nodes algorithm \ref{alg:sddreduction} is executed with linear complexity.

The same behaviour of this procedure can be achieved using an SDD base and simply \textit{inserting} the diagrams, without computing any meld. This is the usual approach and it is also used by \citeauthor{loekito} in \cite{loekito} and \citeauthor{denzumi} in \cite{denzumi, suffixdd}. In this case the complexity is still linear but on average. A comparison of these two approaches is discussed in the next chapter.

\margin{Cite something? Actually the $reduced$ function causes random accesses, but can be removed?}
The present algorithm may be optimized in many ways, and it has a good behaviour on disk since it is essentially based on sorting. For example if the input is very big, instead of computing the reduction in one step, it is possible to do it in blocks, taking benefit of the greater speed of the lower levels of the memory hierarchy. Obviously the additional cost is the computation of further reductions involving the same nodes.



\section{Indexing substrings}

Here an application of SDDs is studied a bit more in depth. The problem is, given a text $T$ of length $N$, to store all the substrings of $T$ inside an SDD. Just to give some numbers on the amount of information stored in such a data structure, the number of different substrings is bounded by:
\begin{align}
\sum_{l=1}^N N -l + 1 = N^2 + N - \frac{N(N+1)}{2} = \frac{N(N+1)}{2} = O(N^2)
\end{align}
\noindent (assuming all symbols are different, normally it is not the case, but asymptotically this is the situation), and their total length is bounded by:
\begin{align}
\sum_{l=1}^N l(N -l + 1) = (N+1)\sum_{l=1}^N l - \sum_{l=1}^N l^2 = O(N^3)
\end{align}

A first algorithm for the construction of an SDD for all the substrings is given in \cite{suffixdd, suffixddnew}, with complexity of $O(N^2)$ on average. But the most interesting fact shown in this work is that the size of such SDD is actually linear in $N$, for isomorphism with DAWG (directed acyclic word graphs). The construction of a DAWG for the substrings of a text is also linear in the size of the text \cite{blumer}, even though the algorithm itself is quite complex. Anyway the problem of constructing an SDD with all the substrings of a given text has already a linear complexity, thanks to the transformations between SDDs and DAWGs.

Being linear in size, the application of SDDs to substrings is really interesting, but a linear time direct construction algorithm does not yet exist. Another algorithm will be now described in this section, and analysed experimentally in the next chapter. A theoretical bound of $O(N^2)$ on its complexity easily holds, but there are strong reasons to believe that the algorithm is actually linear in the size of the input (expected case, since hash tables are used). The theoretical analysis to show this is quite complex and at the moment is not available. Anyway it is a much more natural approach than the one presented in \cite{suffixddnew, suffixdd}.

The algorithm starts inserting in a SDD base the string $T$ and all its prefixes: such a structure will surely exist in the output. To do this it suffices to insert the nodes representing the whole string, then setting to \sinkh{} their LO edges (except the root, since we do not want the empty string).
The root of this diagram is called $P$ in what follows.
Now other $N-1$ nodes are added: each one has LO equal to \sinkl{} and HI pointing to the node in the other chain that follows in the input. This additional nodes represent intuitively the suffixes that have to be added to the other diagram. All the nodes added in this way are not reachable from the root $P$, and in fact they will be used to compute $N-1$ melds with the $P$ diagram. A picture of this structure is presented in figure \ref{fig:sddallsubstrings}.

\input{figures/allsubstrings}

This structure can be easily found considering an input string with all different characters. In this case the structure just built represent almost directly the $\frac{N(N+1)}{2}$ substrings of this scenario. In fact it is sufficient to link together the nodes of the second chain through their LO edges. Also this situation is shown in figure \ref{fig:sddallsubstrings}.

Clearly this last construction is not valid if there are repeated symbols in the input string, because we are creating LO links between vertices that have potentially the same label, therefore the output would not be a SDD. In fact in the general approach they are not linked together but they appear in a SDD base.

Given the described construction, the algorithm \ref{alg:allsubstrings} produces an SDD with all the substrings. Since the pointers in the SDD base are all the suffixes of $T$ (and all their prefixes), it is trivial to prove that the algorithm effectively generates the desired output.

\begin{algorithm}[h]
\SetAlgoLined
%\DontPrintSemicolon
\KwIn{A string $T$}
\KwOut{A pointer $P$ to the SDD with all the substrings of $T$}
\SetKw{KwInn}{in}
\BlankLine
initialize $B$ as described in the text\;
let $S_i$ be the suffixes, for $i = 0,\ldots, N-1$\;
\For{$i\leftarrow N-1$ \KwTo $0$}{
	$P \leftarrow meld( P, S_i, + )$\;
}
\caption{construction of the SDD for all substrings}
\label{alg:allsubstrings}
\end{algorithm}

The only complexity bound that is given here is the following.
\begin{thm}
Given a string of length $N$, algorithm \ref{alg:allsubstrings} computes the output SDD containing all the substrings in at most $O(N^2)$ steps.
\end{thm}
\begin{proof}
This can be easily proved considering the size of the diagrams involved in the melds. The total size of the diagrams $S_i$ is $O(N^2)$ by construction. Melding diagrams of total size $O(N^2)$ has the same complexity, given the optimizations of the meld algorithm discussed in chapter \ref{cha:bdds}.
\end{proof}

The strong argument that suggest the linearity of this algorithm is the presence of many, really many shared nodes between the diagrams $S_i$ that are going to be melded with $P$. This is also supported by experimental results as shown in the next chapter. 

Just to give an idea of the power of this construction, texts of about $10^6$ symbols can be processed in few seconds. In even less time it is possible for example to discover the number of different substrings, thanks to the methodologies described in chapter \ref{cha:bdds}.

At least on a theoretical point of view, SDDs become a good competitor of suffix trees and suffix arrays, being maybe a better choice from the combinatorial side, as just explained. After storing in a SDD all the substrings of $T$, the following operations are easy, and have at least\footnote{Maybe clever algorithms for the specific problem exist.} similar complexity to suffix trees:
\begin{description}
\item[Substring existence] that is, check if a string $Q$ is substring of $T$ or not. If the length of $Q$ is $m$ this operation is performed in at most $O(|\sigma | m )$ time. It can be solved simply traversing the BDD from the root following the path induced by the symbols of $Q$. The same problem has similar complexity $O(m)$ on suffix trees. 
\item[Longest common substring] find the longest common substring of $P$ and $Q$, in $O(m + n)$, given that $|P| = m$ and $|Q| = n$. This can be achieved similarly to the previous situation, counting how many different nodes there are on the two paths induced by $P$ and $Q$.
\item[Longest repeated substring] find the longest repeated substring in $O(N)$, labeling with the appropriate information every node of the diagram.
\end{description}

Many other problems become feasible considering variations of the SDD that for example imply the association of positions or weight to the nodes, as partially done in \cite{loekito}.

\section{Indexing substrings of a given length}

Odd enough, indexing substrings of length shorter than a fixed length $l$, is a more complex problem in terms of decision diagrams, in comparison to indexing \textit{all} the substrings. And it will be shown experimentally that in this situation the compression of the SDD is not so useful, but often is better to simply use an SDD base, for example like the one generated with algorithm \ref{alg:sddbaseconstruction}. %If not specified in this section the term ``SDD base'' will be used to denote this particular scenario.

While SDDs in general are good choice both for storing prefixes and suffixes (the previous section is an example), the situation does change considering SDD bases. The following theorems try to describe this difference (for simplicity the two sinks are not considered in the calculations). In particular the SDD base computed with the reduction algorithm is an easy way to store suffixes among strings, but it is a very bad approach for storing prefixes. %In particular everything is fine considering only suffixes.

\begin{thm}\label{sddbasesuffixes}
Given a string of length $N$, the SDD base for its suffixes (in the sense of algorithm \ref{alg:sddbaseconstruction}) contains no more than $N$ nodes.
\end{thm}
\begin{proof}
Suppose to have an SDD base containing only the initial string $T$. Every suffix of $T$ shares all its nodes with the already existing ones, therefore except for the $N$ nodes of the initial string, no other node is needed (just the root pointers).
\end{proof}

The situation instead is not really satisfying for what concerns prefixes. While the SDD for storing the set of prefixes of a string is linear in the length of the string, the SDD base has quadratic size.

\begin{thm}\label{sddbaseprefixes}
Given a string of length $N$, the SDD base for its prefixes (in the sense of algorithm \ref{alg:sddbaseconstruction}) contains at most $\frac{N (N + 1)}{2}$ nodes.
\end{thm}
\begin{proof}
To reach the upper bound let's assume the the input text does not contain any repeated sequence of symbols. In this case all the prefixes do not share any common suffix, and similarly to the previous proof, every suffix of length $l$ now needs $l$ new nodes to be represented in the base. The total sum is therefore:
\begin{align}
\sum_{l=1}^N l = \frac{N (N + 1)}{2}
\end{align}
\end{proof}

From the previous theorems is possible to prove the following result, that has no counterpart in the SDD world.

\begin{thm}\label{sddbasesubstrings}
Given a string of length $N$, the SDD base for all its substrings of length at most $l$ (in the sense of algorithm \ref{alg:sddbaseconstruction}) contains $O(lN)$ nodes.
\end{thm}
\begin{proof}
Let $T$ be the input string. Assuming that all the $N - l + 1$ substrings of length $l$ do not share any common suffix, the number of needed nodes in the SDD base is $l(N - l + 1)$. All the substrings in $T[l,N)$ are suffixes of some suffix that is assumed to be already present in the base, therefore no additional node has to be added for them. For what concerns instead the substrings of $T[0,l)$, it's enough to add the prefixes of $T[0,l)$, verifying the same property. Since, for the previous theorem \ref{sddbaseprefixes} these prefixes occupy at most $\frac{l (l + 1)}{2}$, the total size is at most:
\begin{align}\label{eqn:l-substrings}
l(N - l + 1) + \frac{l (l + 1)}{2} = \frac{l(N + 2)}{2} = O(lN)
\end{align}
\end{proof}

%But in some situations having an SDD base containing many single sequences, instead of an SDD (base or not) containing the \textit{union} (computed via the melding algorithm), is more space efficient. This is the case for example for the suffixes, for the reason explained above: the SDD naturally share common suffixes.

This theorem has no counter part because the linearity of the substring index in \cite{suffixdd, suffixddnew} has been proved when \textit{all} the substrings are stored, not just until length $l$. What happens ``in the middle'' it is not studied exhaustively, but it is possible that the same result as above holds also for SDDs.

Also this aspect is supported by experimental results.

%However many examples shows that, in general, the melded SDD is smaller than the SDD base.

%For what concerns the creation instead, it is possible to follow the construction of theorem \ref{sddbasesubstrings} obtaining an algorithm with worst case complexity of $O(l N)$, both in time and space of course.

%\margin{Expand a bit. Is really correct?}
%A construction similar to theorem \ref{sddbasesubstrings} leads also to a very simple construction algorithm for the meld approach, in the case where all the substrings of any length are needed. In this sense this approach is not worse than the work in \cite{suffixdd} where a for solving the same problem, a construction of expected complexity $O(N^2)$ is presented. Instead of considering the prefixes, now suffixes are considered. The SDD naturally shares prefixes therefore in computing the meld of the prefixes, all the necessary nodes are already pointers (it is only a matter of rerouting the pointers). To see the complexity of this operation suffice to consider that, given an input of length $M$, an SDD can be constructed in expected time $O(M)$ \cite{denzumi}. Since here the input is set of suffixes of total size $O(N^2)$ what has been claimed follows immediately.

%it is very likely that the problem of building an SDD for \textit{all} the substrings is feasible in $O(N)$, worst case complexity. The authors of \cite{suffixdd} actually already shown that the performance of their algorithm is actually linear on most inputs, although they could not give a formal proof of this behaviour (therefore it would only a matter of improving their analysis, possibly).











%approach is to create all the substrings of the input text $T$, and then run the reduction algorithm \ref{alg:sddbaseconstruction}. The size of all the substrings of a text $T$, limited to length $l$, is:
%\begin{align}
%\sum_{i=1}^l i(N - i + 1) = (N+1) \frac{l(l+1)}{2} - \frac{l(l+1)(2l+1)}{6} = O(l^2 \, N)
%\end{align}
%\noindent while the reduced output is in size $O(lN)$. 
%it is to do a bit better than this, without inserting all the substrings, but just all the prefixes of all the substrings of length $l$. In this scenario every other substring will be a suffix of these ones, and the construction correctness follows from theorem \ref{sddbasesuffixes}; the complexity does not change asymptotically anyway. Probably a better algorithm exist, but we didn't investigate any further.

%\begin{thm}
%Given a string of length $N$, the nodes of the SDD base for all its substrings of length at most $l$ (in the sense of algorithm \ref{alg:sddbaseconstruction}) can be generated in $O(l^2\,N)$ in time and space, reducing all the prefixes of all the substrings of length $l$.
%\end{thm}
%\begin{proof}
%The substrings of lenght $l$ in the input are $N-l+1$, and for each one of them all the prefixes are considered. The total number of prefixes hence is:
%\begin{align}
%(N - l + 1) \sum_{i=0}^l i = (N - l + 1) \frac{l(l+1)}{2} = O(l^2\, N)
%\end{align}
%And the complexity considerations follow. These strings are enough since every other substring of length at most $l$ is a suffix of these ones, and from theorem \ref{sddbasesuffixes} no other node will be added.
%\end{proof}

%To build the SDD for the set of all substrings of a given string $T$, it is possible to start from the situation where all the symbols are different. This situation is pictured in the SDD in figure \ref{boh}. This can be formally demonstrated, but it is very intuitive since the diagram can be semantically divided in two parts:
%\begin{enumerate}[i)]
%\item the right part contains the string itself and all the prefixes. This structure is always present, also if the strings contains some repeated symbols.
%\item the left part contains the suffixes and all their prefixes. This structure, in the case there are repeated symbols in the text, is the one that changes.
%\end{enumerate}
%
%Simply applying this structure to a string with repeated symbols results in diagram formally wrong, because in the left part there are LO edges that links together nodes with the same symbol.
%But it is possible to reason in a different way. Consider the SDD base in figure \ref{boh}, that is the same structure as before, but where the suffixes have been unlinked from the rest of the structure and handled in SDD base fashion.
%The melding (in the sense of union) of $p$ with $s_i$, for $i = 1, \ldots, N$, gives as result the set of all substrings of $T$, for the definition of $p$ and $s_i$.
%
%The point is that, at first glance the complexity of these $N-1$ melds is quadratic, because every diagram involved is linear in size. But this can be refined, also because as one may think, each of the single $s_i$ diagrams is of size $O(i)$, but they share also $O(i)$ nodes with the other diagram where the melding take place.
%
%\margin{L'algoritmo esegue i meld dal basso verso l'alto, quindi da $s_1$ a $s_{N-1}$.}
%In fact, given algorithm \ref{boh} and the optimized meleding \ref{boh} the following theorem holds.
%
%\begin{thm}
%Every meld $s_i \diamond p$, for $i = 1, \ldots, N-1$ causes a constant number of steps before ending up, eventually, in a meld of two nodes of the prefixes chain.
%\end{thm}
%\begin{proof}
%In this proof will be used the notation of definition \ref{boh} to indicate the fields of every considered SDD node. Also a meld is called \textit{trivial} if it is of the form $\bot \diamond x$ or $x \diamond \bot$, because is easily replaced by $x$ in both cases.
%The proof is by induction. 
%
%For $i=1$ it is immediate to show that in every symbol configuration it is necessary at most one step to reach the thesis, since initially $\lo[p] = \bot$. 
%In fact assume that $\lambda(s_1) > \lambda(p)$. The recursion of algorithm \ref{boh} produces the two recursive calls $s_1 \diamond \bot$ and $\bot \diamond \hi[p]$, and both of them are trivial. The same holds for the case where $\lambda(s_1) > \lambda(p)$. The only situation to be different is when $\lambda(s_1) = \lambda(p)$, reaching the two recursive calls $\bot \diamond \bot$ and $\top \diamond \hi[p]$. The first is trivial, while the second is a meld of two nodes of $p$.
%
%Assuming the thesis valid untin step $i-1$, let's consider the meld $s_i \diamond p$. We refer the root of the diagram as $p$ also if it would be better to call it $p'$, since the LO part of the diagram is of course changed after $i$ insertions. Also notice that $\hi[s_i] = y$ is a node of the prefixes chain, for construction of $s_i$.
%
%We start from the case $\lambda(s_i) < \lambda(p')$, that is the easiest one because it ends up in two recursive calls $\bot \diamond p'$ and $y \diamond \bot$, both of them trivial.
%
%Instead if $\lambda(s_i) < \lambda(p')$ the recursion continues on the LO side of $p'$, reaching in at most $\sigma$ steps a node $x$ where $\lambda(s_i) \geq \lambda(x)$ (eventually $\bot$, and this is an easy case). At this point is important to notice that $\hi{x}$ is a node of the prefixes chain because it represent the prefixes of a previously inserted suffix strating with $\lambda{x}$.
%If $\lambda(s_i) > \lambda(x)$, the new recursive calls are $\bot \diamond x$ and $y \diamond \bot$, both of them trivial. While if $\lambda(s_i) = \lambda(x)$ the recursion they are $\bot \diamond \lo{x}$ and $y \diamond \hi{x}$: the first one is trivial, while the second involves nodes of the prefixes chain. We arrived here with at most $\sigma +1$ steps.
%
%Finally consider the case where $\lambda(s_i) = \lambda(p')$. Also here the thesis holds because the first recursive calls are $\bot \diamond \lo{p'}$ and $y \diamond \hi{p'}$. While the first is trivial, the second one involves only node of the prefixes chain.
%\end{proof}
%
%Therefore from this theorem the number of steps performed by the algorithm is at most $\sigma$ times the complexity of melding $N$ pairs of nodes of the prefixes chain.
%
%\begin{thm}
%Assuming the melding of the algorithm are bottom up (from $s_1$ to $s_{N-1}$), the total number of steps performed in meldings of the form $x \diamond y$, where $x$ and $y$ are nodes of the prefixes chain, is $O(N)$.
%\end{thm}
%\begin{proof}
%The proof has three cases. Suppose $\lambda (x) < \lambda(y)$. In these cases the recursive calls are of the form $\top \diamond y$ and $\hi{x} \diamond \bot$, that is trivial. The first one instaed now has $\lambda(\top) > \lambda y$, therefore the two consequent calls are $\top \diamond \top$ and $\bot \diamond \hi{y}$ that are trivial.
%The same argument holds starting with $\lambda (x) > \lambda(y)$.
%
%The situation is totally different when $\lambda (x) = \lambda(y)$. %Here the recursion goes down on the HI edges, continuing melding nodes of the prefixes chain. Eventually this recursion cost $O(N)$ if all the nodes have the same label.
%%But if this is the case, since the algorithm proceeds from bottom to top, the already computed results will be used, because we are in a SDD base. Therefore, also if all the nodes on the prefix chain are equal in label, the total number of steps needed by the melding is linear in $N$.
%\end{proof}
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%








