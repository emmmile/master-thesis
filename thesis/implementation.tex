\label{cha:implementation}

Here will be described a small ZDD/SDD library that has been implemented and called \code{zdd++}. It consists of very limited number of \cplusplus{} classes that implement ZDDs as presented in chapter \ref{cha:bdds}. The most important classes are:

\begin{description}
\item[\code{node}] It represents a node in a decision diagram.
\item[\code{zdd}] It represents a ZDD, and provides all the needed operators, constructors and functions.
\item[\code{sdd}] It represents a SDD, providing the necessary operations as above.
\item[\code{zddbase}] It represents a multi-rooted ZDD, approach described in chapter \ref{cha:bdds}.
\item[\code{sddbase}] It represents a multi-rooted SDD, as above.
\end{description}

In the following we will present briefly all these classes giving both an idea of their implementation, but also a mini how-to for their usage.

\section{Class \code{node}}\label{sec:node}
Class \code{node} implements a tripe $\langle v, l, h \rangle$ in two ways, with different sizes:
\begin{description}
\item[8 bytes] In this case a node is implemented with a 64 bits word as suggested by \citeauthor{knuth4.1} (this has been introduced in section \ref{sec:buildingbdds}). For accessing the three fields in the node bitwise operations are needed.
\item[12 bytes] Here instead it is used an array of 3 \code{uint32\_t}. While this approach uses additional memory it is easier to access the specific fields.
\end{description}

In some early benchmarks the 8-bytes nodes seemed to be better (not by a big margin) and therefore they were the preferred method for building ZDDs and SDDs. In any case it is possible to switch from one implementation to the other setting a e compile time flag.


\section{Class \code{zdd}}
It implements the diagram as a \code{sdt::vector} of \code{node}s, together with a root pointer. All the famous functions like meld and reduce are implemented here, and in general this class can be used to compute boolean functions on a stack.

The constructors are quite intuitive and allow very simple one line definitions. Since ZDDs are often used in association with the encoding \ref{def:naive}, one way to define the ZDD is simply to specify the family of subsets it represents, where the sets are made of integers. The specified indices, and only these ones, will be contained in the family.
For example the ZDD in example \ref{fig:kernelszdd} can be implemented with the following line of code, that is essentially the equation \ref{eqn:kernels}:
\begin{lstlisting}[style=customcplusplus, captionpos=b]
zdd kernels = { {1,3,5}, {2,4,6}, {1,4}, {2,5}, {3,6} };
\end{lstlisting}

Another way of defining functions that is often needed, is to specify some variables and then choose all the possible configurations of the remaining ones, where the ``remaining ones'' is specified through a maximum index (the minimum is assumed to be 1). Clearly this is useful when the number of free variables is enough to make the number of binary configurations explode. This line of code for example:
\begin{lstlisting}[style=customcplusplus, captionpos=b]
zdd term ( { {1,2} }, n );
\end{lstlisting}
\noindent implements the boolean function $f(x_1, x_2, \ldots, x_n) = x_1 x_2$. This is basically realized setting the proper structure for the specified variables and then implementing any possible configuration for the other ones. The latter is very easy in terms of the diagram construction, while the former needs one step of melding for every specified set.

\begin{figure}[p]
\lstinputlisting[style=customcplusplus, captionpos=b]{../code/coolexample.cpp}
\npthousandsep{$\,\,$}
\caption[Mini tutorial for the \code{zdd} class]{A simple usage example for the \code{zdd} class. The last number printed from this example is \numprint{4283494371512410}, and its computation takes lass than a second.}
\label{code:zddexample}
\end{figure}

It has been implemented also a procedure for counting the number of solutions of a given \code{zdd} instance. Since the number of solutions can be extremely high also for small ZDDs, the GNU multiprecision arithmetic library (GMP) has been used for this purpose. 

A comprehensive example is presented in figure \ref{code:zddexample}, where the kernels of the cyclic graph $C_n$ are computed.

Since sometimes is useful also to have a graphical feedback, the \code{zdd} class also have some functions for exporting the diagram in a format compatible with Wolfram Mathematica, that includes very powerful algorithms for graph drawing (in particular for node positioning). A small Mathematica script for drawing these diagrams is also included in the package, and a sample is presented in figure \ref{fig:mathematicasample}.

\input{figures/mathematicasample}

Many operations that are performed on ZDDs require some auxiliary memory space to efficiently solve some subproblems. The most important example is the melding operation that uses an hash table as cache to remember the input pairs that algorithm already processed. 

Hash tables, especially in the implementation present in the \cplusplus{} 2011 standard, \code{std::unordered\_map}, use heavily the heap because they use linked lists for storing the items in the buckets. Therefore many allocations and deallocations are necessary to handle this data structure.
Since in some cases these hash tables are used as \textit{memo} or as auxiliary data structure it is possible to save many calls to operators \code{new} and \code{delete}, considering to use \cplusplus{} allocators \cite{allocators}.

%The predefined allocator corresponds to an invocation of the \code{new} operator. Instead we can define a custom allocator object that uses a big predefined memory area where all the accesses needed for the cache hash tables are done. Custom allocators can be passed as template arguments of practically all \cplusplus{} containers present in the standard library.
In particular is possible to define a custom allocator that uses a big memory area as a stack. Requesting for a new address cause a pointer to increase its position in the stack. After executing an operation, like \code{meld()} the memory area is cleared and it is ready for other operations. Here clearing, doesn't mean deallocating, like in the normal case, but simply means to reset the stack pointer to its initial position. %In some sense this method become very similar to the Knuth's melding algorithm presented in \cite{knuth4.1}.

\bigskip
In this way it is possible to control the heap usage of the application even in situations where complex auxiliary data structures have to be used (without having to recode the whole data structure).

\section{Class \code{sdd}}
There is not much to add in respect to the \code{zdd} class, since this is basically an extension, from the object oriented programming point of view, of the previous class.

The only difference are the constructors that for simplicity of usage are string oriented. A simple SDD can be built from a set of strings, in a similar way to the ZDD case:
\begin{lstlisting}[style=customcplusplus, captionpos=b]
sdd words = { "this", "is", "a", "nice", "implementation" };
\end{lstlisting}


\section{Class \code{zddbase}}
This class implements the other approach for handling ZDDs that is using an \textit{unique table} as explained in chapter \ref{cha:bdds}. There were some possibilities for the implementation of this class. For using a \code{node} structure as the one described in the previous section \ref{sec:node}, that is avoiding pointers, is necessary to store the nodes in an auxiliary data structure given a node index return the associated triple.% Except for the unique table that is a mapping between node triples and pointers/indices 

Summarizing the implementation options were:
\begin{enumerate}[i)]
\item Use real pointers. In this case a node would have size at least 20 bytes on a 64 bit architecture, excluded overheads related to the involved data structures.
\item Use an \code{std::unordered\_map} as unique table and a \code{std::vector} for storing the nodes. Excluding overheads this means to use at least $4 + 8 = 12$ bytes per node, if the Knuth approach is used.
\item Use a \code{boost::bimap} that directly solves the problem of storing the mapping in both directions. The implementation of \code{boost::bimap} is not very easy to understand and the documentation lacks on the details about what is actually stored, but assuming that it is implemented with two \code{std::unordered\_map} the space usage is at least 12 bytes, again.
\end{enumerate}

Since \code{std::unordered\_map} has an overhead of at least a pointer per bucket, the space utilization per node is actually 28 bytes for the first option, 20 bytes for the second and 28 bytes for the third.
Clearly the second implementation is not really dynamic, removing nodes cause ``holes'' to remain in the vector, and periodic reconstructions would be needed in order to maintain the data structure clean.
But except this aspect the second option should be equally valid, and superior in terms of locality in respect to the first possibility. %The first option is the one widely used when we are speaking about BDD bases, but a garbage collection mechanism is needed to make it really dynamic.

In practice the third option is faster than the second one, and since the already defined class \code{node} can be used seamless in this case, this is the implementation that has been chosen. No particular garbage collection mechanism has been implemented for deleting unnecessary nodes, in order to perform better in the benchmarks of the previous chapter. Periodically a reconstruction is needed to delete all the nodes that for example are not reachable from a specific root node.

Similar algorithms to the works of \cite{denzumi, minato, suffixdd, loekito} are implemented here, so nothing special to say about this.

\section{Class \code{sddbase}}
As in the hierarchy between \code{zdd} and \code{sdd} an \code{sddbase} is simply a \code{zddbase} with different constructors. All other algorithms and functions are the same.

Actually two implementations are given, in order to realize also algorithm \ref{alg:sddbaseconstruction} of chapter \ref{cha:sdds}. In this case some functionalities are really different. For example the reduction is not implicit as in the normal SDD base, but a proper function \code{reduce()} implements the reduction algorithm \ref{alg:sddbaseconstruction}.