# master-thesis

[This](https://gitlab.com/emmmile/master-thesis/-/jobs/artifacts/master/raw/thesis/thesis.pdf?job=build:pdf
) is my master thesis.

There is also a lot of code related to binary decision diagrams in here that could be useful in the future.
