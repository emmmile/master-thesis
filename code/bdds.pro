TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    bdds/test-zdd.cpp \
    bdds/test-sdd.cpp \
    strings/test-strings.cpp \
    coolexample.cpp \
    utils/example.cpp \
    subsequences.cpp \
    bdds/test-cudd.cpp \
    random.cpp \
    utils/bimap-implementation.cpp \
    suffixes.cpp \
    linebyline.cpp \
    demonstration.cpp

HEADERS += \
    strings/alphabet.hpp \
    bdds/node.hpp \
    bdds/zdd.hpp \
    bdds/sdd.hpp \
    strings/bitstring.hpp \
    utils/stackallocator.hpp \
    strings/static.hpp \
    utils/utils.hpp \
    utils/bignum.hpp \
    bdds/sddbase.hpp \
    subsequences.h \
    bdds/sharedsdd.hpp

OTHER_FILES += \
    plot.sh \
    CMakeLists.txt \
    benchmark.py

