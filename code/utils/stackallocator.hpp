#ifndef STACK_ALLOCATOR_HPP
#define STACK_ALLOCATOR_HPP

#include <memory>
#include <algorithm>
#include <cassert>
#include <cstring>
using namespace std;


// http://home.roadrunner.com/~hinnant/stack_alloc.html

///
/// Implements a custom allocator that uses a single area of memory.
/// Deallocation does nothing, it simply allocates until the end.
/// Therefore once in a while it's necessary to clean everything and
/// start again from the beginning.
///
/// The first two positions in the array contain special data:
///   - an index in the array for the first free position
///   - a reference counter, because this structure can be reinterpreted
///     many times, for example in the STL containers. Therefore the same
///     array is actually used by many instances of this allocator.
///     (then: when to actually deallocate everything?)
///
template<class T>
struct stackallocator {
    T*   __base;
    static const std::size_t __size = 4 * 1048576; // bytes

    typedef T value_type;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    template< class U > struct rebind { typedef stackallocator<U> other; };



    std::size_t index ( ) const {
        return reinterpret_cast<std::size_t*>(__base)[0];
    }

    std::size_t setindex ( std::size_t index ) {
        return ( reinterpret_cast<std::size_t*>(__base)[0] = index );
    }

    std::size_t counter ( ) const {
        return reinterpret_cast<std::size_t*>(__base)[1];
    }

    std::size_t setcounter ( std::size_t counter ) {
        return ( reinterpret_cast<std::size_t*>(__base)[1] = counter );
    }

    stackallocator( ) : __base( 0 ) {
        //cout << "stack_allocator()" << endl;
        __base = static_cast<T*> (::operator new (__size) );
        setindex( sizeof( std::size_t ) * 2 );
        setcounter( 1 );
        clear();
    }

    stackallocator( const stackallocator& other )
        : __base( other.__base ) {
        setcounter( counter() + 1 );
        //cout << "stack_allocator(...) " << endl;
    }

    template< class U >
    stackallocator( const stackallocator<U>& other )
    // cast from U* to T*
        : __base( reinterpret_cast<T*>(other.__base) ) {
        setcounter( counter() + 1 );
        //cout << "stack_allocator(...) " << endl;
    }

    ~stackallocator ( ) {
        setcounter( counter() - 1 );
        if ( counter() == 0 ) {
            //cout << "~stack_allocator() "  << endl;
            ::operator delete(__base);
        }
    }

    void clear() {
        setindex( sizeof( std::size_t ) * 2 );
        memset( reinterpret_cast<char*>(__base) + index(), 0, __size - index() );
    }

    pointer address( reference x ) const {
        return &x;
    }

    pointer allocate( size_type n, std::allocator<void>::const_pointer hint = 0 ) {
        assert( index() + sizeof( T ) * n < __size );
        pointer out = reinterpret_cast<T*>( reinterpret_cast<char*>(__base) + index() );
        setindex( index() + sizeof( T ) * n );
        return out;
    }

    size_type max_size() const {
        return __size;
    }

    template< class U >
    void destroy( U* p ) {
    }

    void deallocate(pointer p, size_type n) {
    }

    void construct( pointer p, const_reference val ) {
        new((void *)p) T(val);
    }

    template< class U, class... Args >
    void construct( U* p, Args&&... args ) {
        ::new((void *)p) U(std::forward<Args>(args)...);
    }
};

#endif // STACK_ALLOCATOR_HPP
