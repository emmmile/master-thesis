#ifndef VARIOUS_HPP
#define VARIOUS_HPP

#include <algorithm>
#include <functional>
#include <list>
#include <map>
#include <set>
#include <string>
using namespace std;

// template<class random_iterator>
// inline random_iterator unique_resize ( random_iterator begin, random_iterator
// end ) {
//  random_iterator newend = unique( begin, end );
//  in.resize( distance( in.begin(), newend ) );
//  return in;
//}

template <class random_iterator>
inline size_t sort_unique(random_iterator begin, random_iterator end) {
  sort(begin, end);
  return unique(begin, end) - begin;
}

// trim from start
template <class str>
static inline str& ltrim(str& s) {
  s.erase(s.begin(),
          find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
  return s;
}

// trim from end
template <class str>
static inline str& rtrim(str& s) {
  s.erase(
      find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
      s.end());
  return s;
}

// trim from both ends
template <class str>
static inline str& trim(str& s) {
  return ltrim(rtrim(s));
}

// set and list printer
template <typename T>
std::ostream& operator<<(std::ostream& stream, const std::list<T>& map) {
  stream << '{';
  char comma[3] = {'\0', ' ', '\0'};
  for (const auto& i : map) {
    stream << comma << i;
    comma[0] = ',';
  }
  stream << '}';
  return stream;
}

// set and list printer
template <typename T, typename Compare>
std::ostream& operator<<(std::ostream& stream,
                         const std::set<T, Compare>& map) {
  stream << '{';
  char comma[3] = {'\0', ' ', '\0'};
  for (const auto& i : map) {
    stream << comma << i;
    comma[0] = ',';
  }
  stream << '}';
  return stream;
}

template <typename T, typename Compare>
std::ostream& operator<<(std::ostream& stream,
                         const std::map<T, T, Compare>& map) {
  stream << '{';
  char comma[3] = {'\0', ' ', '\0'};
  for (const auto& i : map) {
    stream << comma << i.first << " : " << i.second;
    comma[0] = ',';
  }
  stream << '}';
  return stream;
}

#endif  // VARIOUS_HPP
