#ifndef BIGNUM_HPP
#define BIGNUM_HPP

#include <ostream>
#include <random>
using namespace std;

// eventually support allocators, if I will ever use higher precision


class bignum {
    unsigned __int128 n;
public:
    bignum( ) : n( 0 ){ }

    template<class T>
    bignum( const T& x ) : n( x ) { }

    bool operator< ( const bignum& another ) const { return n < another.n; }
    bignum operator+ ( const bignum& another ) const { return bignum(n + another.n); }
    bignum operator- ( const bignum& another ) const { return bignum(n + another.n); }
    bignum operator* ( const bignum& another ) const { return bignum(n * another.n); }
    bignum operator/ ( const bignum& another ) const { return bignum(n / another.n); }
    bignum& operator+= ( const bignum& another ) { n += another.n; return *this; }
    bignum& operator-= ( const bignum& another ) { n -= another.n; return *this; }
    bignum& operator*= ( const bignum& another ) { n *= another.n; return *this; }
    bignum& operator/= ( const bignum& another ) { n /= another.n; return *this; }

    /*template<class T>
    bignum& operator %( const T& b ) {
        for(i = 0, i<a.length; i++) {
            result *= (256 % b);
            result %= b;
            result += (a[i] % b);
            result %= b;
        }
    }*/

    template<class G>
    static bignum rand ( const bignum& mod, G& generator ) {
        unsigned __int128 out = 0;
        //out.n = generator() << 64 | generator();

        size_t iterations = sizeof( unsigned __int128 ) / sizeof( typename G::result_type );
        for ( size_t i = 0; i < iterations; ++i )
            out |= generator() << (8 * i * sizeof( typename G::result_type ));


        //cout << out << " " << mod << endl;
        //getchar();
        return out %= mod.n;;
    }

    //http://stackoverflow.com/questions/11656241/how-to-print-uint128-t-number-using-gcc
    friend ostream& operator<<(std::ostream& stream, const bignum& v ) {
        unsigned __int128 n = v.n;

        if (n == 0) return stream << "0";
        char str[40] = {'\0'}; // log10(1 << 128) + '\0'
        char *s = str + sizeof(str) - 1; // start at the end
        while (n != 0) {
            *--s = "0123456789"[n % 10]; // save last digit
            n /= 10;                     // drop it
        }

        return stream << s;
    }
};

#endif // BIGNUM_HPP
