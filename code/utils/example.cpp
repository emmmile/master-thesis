
#include <set>
#include <unordered_set>
#include <map>
#include <iostream>
#include <functional>
#include <list>
#include <cstdlib>
#include <gmpxx.h>
#include <boost/timer.hpp>
#include "utils/utils.hpp"
#include "stackallocator.hpp"
#include "bignum.hpp"
using namespace std;
using namespace boost;


// define a custom allocator
typedef stackallocator<int> alloc;

int main ( ) {
    int n = 100000;

    timer t;
    // allocation test, to debug with valgrind
    for ( int j = 0; j < 10; ++j ) {
        //m.clear();
        alloc m;

        unordered_set<int, hash<int>, equal_to<int>, alloc> test( 10, hash<int>(), equal_to<int>(), m );
        for ( int i = 0; i < n; ++i ) test.insert( random() % 1000000 );
    }

    cout << "Elasped time: " << t.elapsed() << endl;
    t.restart();

    alloc m;
    vector<bignum, alloc> longintegers( m );

    for ( int j = 0; j < n / 100; ++j ) longintegers.push_back( j );



    return 0;
}
