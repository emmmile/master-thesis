
#include <iostream>
#include <fstream>
#include <functional>
#include <string>
#include <streambuf>
#include <set>
#include <sstream>
#include <string>
#include "bdds/sddbase.hpp"
#include "strings/static.hpp"
#include <boost/program_options.hpp>
#include <boost/timer.hpp>
#include <fstream>
#include <sys/resource.h>
using namespace std;
using namespace boost;
using namespace boost::program_options;


string randomstr ( const string& sigma, const size_t l ) {
    char out [l + 1];
    for ( size_t i = 0; i < l; ++i )
        out[i] = sigma[random() % sigma.length()];
    out[l] = '\0';

    return string( out );
}

int main( int argc, char** argv ) {
    /*example = sdd::allsubstrings( "aabbaabbaabb" );
    cout << example.size() << endl;
    cout << "Solutions: " << example.count() << endl;
    cout << example << endl;*/
    string input = "input";
    size_t l;

    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce this help message")
            ("input,i", value<string>(&input), "set the location of the input text")
            ("length,l", value<size_t>(&l), "set the length of the subsequence")
            ;

    variables_map vm;

    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            exit( 1 );
        }

        notify(vm);
    } catch ( error& e ) {
        cerr << desc << endl;
        cerr << e.what() << endl;
        exit( 1 );
    }

    if ( !vm.count("input") ) {
        cerr << "option required.\n";
        exit( 1 );
    }

    std::ifstream t(input);
    if ( !t ) {
        cerr << "file \"" << input << "\" not found.\n";
        exit( 1 );
    }
    timer elapsed;

    string line;
    sddbase example;
    node root = example.empty();

    while ( t >> line ) {
        line = trim(line);
        node current = example.insert( line );
        root = example.meld( current, root, binary_or() );
    }

    printf( "Finished in %lf s. ", elapsed.elapsed() ); cout.flush();
    cout << "There are " << example.size(root) << " nodes in the SDD.\n";
    cout << "There are exactly " << example.count(root) << " different solutions.\n";

    /*string line;
    sdd example;

    while ( t >> line ) {
        line = trim( line );
        sdd current( line );
        example = sdd::meld( example, current, binary_or() );
    }

    printf( "Finished in %lf s. ", elapsed.elapsed() ); cout.flush();
    cout << "There are " << example.size() << " nodes in the SDD.\n";
    cout << "There are exactly " << example.count() << " different solutions.\n";

    example.mathematica<char>( cout, false );*/

    return 0;
}

