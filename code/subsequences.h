#ifndef SUBSEQUENCES_H
#define SUBSEQUENCES_H

#include <set>
#include <unordered_set>
#include "strings/static.hpp"
using namespace std;

#ifndef MAX_LENGTH
#define MAX_LENGTH          32
#endif

typedef staticstring<MAX_LENGTH> ss;
typedef unordered_set<ss> sset;
typedef unordered_set<ss>::iterator ssi;
typedef vector<ss> svec;
typedef vector<ss>::iterator svi;
typedef vector<ss>::const_iterator svci;


// removes character at position i in the string in
inline ss& remove( ss& in, const size_t i ) {
    return in.erase( i, 1 );
}

inline svec& unique_resize ( svec& in ) {
    svi newend = unique( in.begin(), in.end() );
    in.resize( distance( in.begin(), newend ) );
    return in;
}

inline svec& sort_and_resize( svec& in ) {
    sort( in.begin(), in.end() );
    return unique_resize( in );
}








// returns true if the sequence sub is contained in the window of width w,
// at position k of the input sequence s
bool contained ( const string& s, const size_t w, const size_t k, const ss& sub ) {
    size_t j = k;
    for( size_t i = 0; i < sub.length(); ++i, ++j ) {
        for ( ; j < k + w; ++j ) if( s[j] == sub[i] ) break;
        if ( j == k + w ) return false;
    }

    return true;
}

void window_sequences ( const string& s, const size_t l, const size_t w, const size_t position, svec& out ) {
    assert( s.length() > w + position );
    //sset* out = new sset;

    // initialize with the first prefix
    out.push_back( s.substr( position, l ) );
    //  svec union_temp;

    //size_t threshold = 16;
    // compute all the other subsequences
    for ( size_t k = l; k < w; ++k ) {
        svec temp;

        for( svi i = out.begin(); i != out.end(); ++i ) {
            for ( size_t j = 0; j < l; ++j ) {
                ss scopy = *i;
                temp.push_back( remove( scopy, j ).append( 1, s[position + k] ) );
            }

            sort_and_resize( temp );
        }


        //    set_union( out.begin(), out.end(), temp.begin(), temp.end(),  std::back_inserter(union_temp) );
        //    out = union_temp;
        //    union_temp.clear();
        out.insert( out.end(), temp.begin(), temp.end() );
        sort_and_resize( out );
    }
}


void swindow_sequences ( const string& s, const size_t l, const size_t w, const size_t position, sset& out ) {
    assert( s.length() > w + position );

    out.insert( s.substr( position, l ) );
    for ( size_t k = l; k < w; ++k ) {
        sset temp;

        for( ssi i = out.begin(); i != out.end(); ++i ) {
            for ( size_t j = 0; j < l; ++j ) {
                ss scopy = *i;
                temp.insert( remove( scopy, j ).append( 1, s[position + k] ) );
            }
        }

        out.insert( temp.begin(), temp.end() );
    }
}



svec& next_step ( const string& s, const size_t w, const size_t k, const svec& in, svec& out, char x, char y ) {
    // takes all strings starting with character x, removing it
    // and adding all the strings that do not start with x
    for ( svci i = in.begin(); i != in.end(); ++i ) {

        // any string starting with x has to be added, removing the starting x
        // and adding a trailing y
        if ( (*i)[0] == x ) {
            out.push_back( *i );
            remove( out.back(), 0 );
            out.back().push_back( y );

            // all other sequences has to be verified, that is,
            // if *i is a valid subsequence alone, I add it without modifications
            if ( contained( s, w - 1, k, *i ) ) {
                out.push_back( *i );
            }
        } else {
            out.push_back( *i );
        }


    }

    sort_and_resize( out );


    //  printf( "The %d-window starting at %d is %.*s\n", int(w-1), int(k), int(w-1), s.c_str() + k );
    //  cout << "and E contains:\n";
    //  for ( auto i : out ) cout << i << endl;
    //  getchar();

    return out;
}

svec all_subsequences ( const string& s, const size_t l, const size_t w ) {
    //  cout << "l = " << l << "\nw = " << w << endl;
    //  cout << s.substr( 0, 30 ) << endl;

    // compute all subsequences of [0, w)
    svec e;
    svec d;
    window_sequences( s, l, w, 0, d );
    window_sequences( s, l-1, w-1, 1, e );
    //sset* tempd = window_sequences( s, l-1, w-1, 1 );
    //next_step( s, w, 1, *tempd, *e, s[0], s[w+1] );


    for ( size_t k = 1; k < s.length() - w; ++k ) {
        char x = s[k];
        char y = s[k + w - 1];

        // add the new subsequences to the set
        //for ( size_t i = 0; i < e->size(); ++i ) d->insert( (*e)[i] = (*e)[i] + y );
        size_t middle = d.size();
        for ( auto i : e ) d.push_back( i + y );
        inplace_merge(d.begin(), d.begin() + middle, d.end());
        unique_resize( d );

        // compute E for the next step
        svec temp;
        e = next_step( s, w, k + 1, e, temp, x, y );
        //sort_and_resize( d );
    }

    return d;
}








void single ( const ss& s, size_t beg, size_t remaining, ss current, svec& out ) {
    if ( remaining == 0 ) { out.push_back( current ); return; }
    if ( s.length() - beg == remaining ) { out.push_back( s.substr( beg, remaining ) ); return; }

    //cout << beg << " " << remaining << endl;

    single( s, beg + 1, remaining, current, out );
    single( s, beg + 1, remaining - 1, current.append(1, s[beg]), out );
}


svec seq ( const ss& s ) {
    svec out;

    for ( size_t i = 0; i < s.length(); ++i ) {
        ss current = s;
        out.push_back( current.erase(i, 1) );
    }

    return out;
}

svec simpleseq ( const string& s, const size_t l, const size_t w ) {
    svec e;
    svec d;

    if ( s.size() == w ) {
        d.push_back( s );
    }

    // initialization
    for ( size_t i = w; i < s.size(); ++i ) d.push_back( s.substr( i - w, w ) );
    sort_and_resize( d );


    // loop
    for ( size_t i = w; i > l; --i ) {
        for ( size_t j = 0; j < d.size(); ++j ) {
            svec current = seq( d[j] );

            e.insert( e.end(), current.begin(), current.end() );
        }

        sort_and_resize( e );
        swap( e, d );
        e.clear();
    }

    return d;
}



void print( vector<size_t>& sequence ) {
    for ( size_t i = 0; i < sequence.size() - 2; ++i ) cout << sequence[i] << " ";
    cout << endl;
}

// size of sequence must be at least t+2
void combinations ( size_t t, size_t n ) {
    size_t j;
    vector<size_t> c;
    c.resize( t + 2, 0 );

    // initialization
    for ( j = 0; j < t; ++j ) c[j] = j;
    c[t] = n;
    c[t+1] = 0;

    j = 0;
    while ( true ) {
        // visit
        print( c );
        // find j
        for ( j = 0; c[j] + 1 == c[j+1]; ++j )
            c[j] = j;

        // done?
        if ( j >= t ) break;

        // increase
        c[j]++;
    };
}



// size of sequence must be at least t+2
void combinationsfast ( size_t t, size_t n ) {
    size_t j, x;
    vector<size_t> c;
    c.resize( t + 3, 0 );

    // initialization
    for ( j = 1; j <= t; ++j ) c[j] = j - 1;
    c[t+1] = n;
    c[t+2] = 0;

    j = t;
    while ( true ) {
        // visit
        print( c );

        if ( j > 0 ) {
            x = j;
        } else {
            if ( c[1] + 1 < c[2] ) {
                c[1]++;
                continue;
            }

            // find j
            j = 1;
            do {
                ++j;
                c[j-1] = j-2;
                x = c[j] + 1;
            } while( x == c[j+1] );

            // done?
            if ( j > t ) break;
        }



        // increase
        c[j] = x;
        j--;
    };
}



#endif // SUBSEQUENCES_H
