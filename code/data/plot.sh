#!/bin/bash

N=32
SIGMA=4

rm plot

echo "#N = $N, SIGMA=$SIGMA" >> plot
for i in $(seq 1 $N); do
    scons -c
    scons define=EL=$i define=ESIZE=$N define=ESIGMA=$SIGMA
    ./main-hash >> plot
done
