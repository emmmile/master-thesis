// g++ example.cpp -o example --std=c++11 -lgmp -lgmpxx

#include <iostream>

#include "bdds/zdd.hpp"
using namespace std;

int main() {
  unsigned int n = 128;
  zdd is({{}}, n);  // all solutions
  zdd ds({{}}, n);
  zdd kernels;

  for (unsigned int i = 1; i <= n; ++i) {
    unsigned int u = i;
    unsigned int z = i == 1 ? n : i - 1;
    unsigned int v = i % n + 1;
    zdd isterm({{}, {u}, {v}}, n);
    zdd dsterm({{i}, {z}, {v}, {i, z}, {i, v}, {z, v}, {i, z, v}}, n);

    is = zdd::meld( is, isterm, binary_and() );
    ds = zdd::meld( ds, dsterm, binary_and() );
  }

  kernels = zdd::meld( is, ds, binary_and() );

  cout << "ZDD size: " << kernels.size() << endl;
  cout << "Kernels:  " << kernels.count() << endl;
}
