#include <iostream>
#include <boost/progress.hpp>
#include <boost/program_options.hpp>
#include <fstream>
using namespace std;
using namespace boost;
using namespace boost::program_options;



string randomstr ( const string& sigma, const size_t l ) {
    char out [l + 1];
    for ( size_t i = 0; i < l; ++i )
        out[i] = sigma[random() % sigma.length()];
    out[l] = '\0';

    return string( out );
}


int main( int argc, char** argv ) {
    size_t l = 12;
    string sigma = "AB";
    size_t maxsize = 4 * 1024 * 1024;

    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce this help message")
            ("length,l", value<size_t>(&l), "set the length of the output strings")
            ("alphabet,a", value<string>(&sigma), "set the alphabet")
            ("truncate,t", value<size_t>(&maxsize), "limit the maximum size of the output")
            ;

    variables_map vm;

    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            exit( 1 );
        }

        notify(vm);
    } catch ( error& e ) {
        cerr << desc << endl;
        cerr << e.what() << endl;
        exit( 1 );
    }

    if (!vm.count("length")) {
        cout << "option --length is required" << "\n";
        cout << desc << "\n";
        exit( 1 );
    }

    for ( size_t i = 0; i < maxsize; i += l )
        cout << randomstr(sigma, l) << endl;

    return 0;
}


