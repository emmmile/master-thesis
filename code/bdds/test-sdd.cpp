
#include <iostream>
#include <fstream>
#include <functional>
#include <string>
#include <streambuf>
#include <set>
#include <sstream>
#include <string>
#include "sdd.hpp"
#include "sddbase.hpp"
#include "sharedsdd.hpp"
#include "strings/static.hpp"
#include <boost/program_options.hpp>
#include <boost/timer.hpp>
#include <fstream>
using namespace std;
using namespace boost;
using namespace boost::program_options;

#ifndef MAX_LENGTH
#define MAX_LENGTH      32
#endif

typedef staticstring<MAX_LENGTH> ss;

string randomstr ( const string& sigma, const size_t l ) {
    char out [l + 1];
    for ( size_t i = 0; i < l; ++i )
        out[i] = sigma[random() % sigma.length()];
    out[l] = '\0';

    return string( out );
}

size_t hashway ( const vector<ss>& strings ) {
    sddbase base;
    base.insert( strings.begin(), strings.end() );
    return base.size();

    /*sddbase base;
    node all = base.empty();
    for ( auto& i : strings ) {
        currentsize += i.length();
        all = base.meld( base.insert( i ), all, binary_or() );
    }

    base = sddbase( base, all, all );
    return base.size();*/
}

size_t hope ( const vector<ss>& strings ) {
    sharedsdd base;
    base.insert( strings.begin(), strings.end() );
    base.reduce();
    //cout << base << endl;
    return base.size();
}

/*std::istream& operator>>(std::istream& is, ss& obj)
{
  // read obj from stream
  if( no valid object of T found in stream )
    is.setstate(std::ios::failbit);
  return is;
}*/

int main( int argc, char** argv ) {
    /*example = sdd::allsubstrings( "aabbaabbaabb" );
    cout << example.size() << endl;
    cout << "Solutions: " << example.count() << endl;
    cout << example << endl;*/

    string input = "input.txt";
    bool mode = false;
    bool benchmark = false;
    size_t trials = 3;


    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce this help message")
            ("input,i", value<string>(&input), "take substrings from a huge random one")
            ("benchmark,b", "don't print anything, just the completion time")
            ("mode,m", value<bool>(&mode), "use hash tables or arrays?")
            ;

    variables_map vm;

    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            exit( 1 );
        }

        notify(vm);
    } catch ( error& e ) {
        cerr << desc << endl;
        cerr << e.what() << endl;
        exit( 1 );
    }

    timer elapsed;

    if (vm.count("benchmark")) {
        benchmark = true;
    }


    std::ifstream infile(input);
    if ( !infile ) {
        cerr << "file \"" << input << "\" not found.\n";
        exit( 1 );
    }

    size_t total = 0;
    vector<ss> strings;
    string current;
    while ( infile >> current ) {
        strings.emplace_back( current.c_str() );
        //cout << current << endl;
        total += current.length();
    }


    if ( !benchmark ) printf( "Read %zu symbols in %lf s.\n", total, elapsed.elapsed() );


    size_t nodes;
    double totaltime = 0.0;
    if ( mode ) {
        for ( size_t i = 0; i < trials; ++i ) {
            elapsed.restart();
            nodes = hashway( strings );
            totaltime += elapsed.elapsed();
        }
        if ( !benchmark ) printf( "Built an hash base of  %zu nodes in ", nodes );
        printf( "%.3lf\n", totaltime / trials );
        //printf( "%zu\n", nodes );
    } else {
        for ( size_t i = 0; i < trials; ++i ) {
            elapsed.restart();
            nodes = hope( strings );
            totaltime += elapsed.elapsed();
        }
        if ( !benchmark ) printf( "Built a shared base of %zu nodes in ", nodes );
        printf( "%.3lf\n", totaltime / trials );
        //printf( "%zu\n", nodes );
    }


    return 0;
}

