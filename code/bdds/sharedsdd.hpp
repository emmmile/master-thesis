#ifndef SHAREDSDD_HPP
#define SHAREDSDD_HPP

#include "zdd.hpp"
#include "sdd.hpp"
#include <boost/timer.hpp>
using namespace std;
using namespace boost;


class sharedsdd {
    typedef bignum               big_type; // mpz_class does not respect allocator specification
    vector<node>                 nodes;
    vector<node::pointer_type>   roots;

public:
    ///
    /// \brief Builds an empty sdd base, containing only the two sinks.
    ///
    sharedsdd() {
    }


    void allsubstrings ( const string& s ) {
        nodes.emplace_back( node::value_max, 0, 0 );
        nodes.emplace_back( node::value_max, 1, 1 );
        int j = 1;

        for ( int i = s.length() - 1; i >= 0; --i ) {
            nodes.emplace_back( s[i], 1, j );
            roots.push_back( ++j );
        }
    }

    template <class InputIterator>
    void insert ( InputIterator first, InputIterator last ) {
        // copy and shift the nodes
        for ( ; first != last; ++first ) {// *first is a string

            nodes.emplace_back( node::value_max, 0, 0 );
            nodes.emplace_back( node::value_max, 1, 1 );

            if ( first->length() == 0 ) {
                roots.push_back( nodes.size() - 1 );
                continue;
            }

            for ( int i = first->length() - 1; i >= 0; --i )
                nodes.emplace_back( (*first)[i], 0, nodes.size() - 1);

            roots.push_back( nodes.size() - 1 );
        }
    }

    void compute_levels ( const node::pointer_type i, vector<entry>& aux ) const {
        if ( nodes[i].val() == node::value_max ) { aux[i].__level = 0; return; }

        node::pointer_type l = nodes[i].lo();
        node::pointer_type h = nodes[i].hi();
        if ( aux[l].__level == 0 ) compute_levels( l, aux );
        if ( aux[h].__level == 0 ) compute_levels( h, aux );

        aux[i].__level = max( aux[l].__level, aux[h].__level ) + 1;
    }

    node::pointer_type compute_levels_tail ( const node::pointer_type i, vector<entry>& aux ) const {
        if ( aux[i].__level != 0 ) { return aux[i].__level; }
        if ( nodes[i].val() == node::value_max ) { return aux[i].__level = 0; }

        return aux[i].__level = max( compute_levels_tail( nodes[i].lo(), aux ),
                                     compute_levels_tail( nodes[i].hi(), aux ) ) + 1;
    }



    ///
    /// \brief reduce
    /// \return
    ///
    void reduce ( ) {
        //vector<node::pointer_type> sorted ( nodes.size() );
        //vector<node::pointer_type> level  ( nodes.size() );
        vector<node::pointer_type> reduced( nodes.size() );
        vector<entry> aux;
        timer elapsed;

        // initialize auxiliary array
        for ( size_t i = 0; i < nodes.size(); ++i ) {
            reduced[i] = node::pointer_max;
            aux.emplace_back( nodes[i], 0, i );
        }

        for ( size_t i = 0; i < roots.size(); ++i )
            compute_levels_tail( roots[i], aux );

        //printf( "initialization: %lf s.\n", elapsed.elapsed() ); elapsed.restart();
        // sort the nodes according just to the level
        sort( aux.begin(), aux.end(), levelsorter() );
        //printf( "first sort:     %lf s.\n", elapsed.elapsed() ); elapsed.restart();


        //        for ( size_t k = 0; k < aux.size(); ++k ) {
        //            cout << aux[k].__node; printf( " %zu\n", aux[k].__level );
        //        }
        //        getchar();

        reduced[0] = 0;
        reduced[1] = 1;
        size_t last = 0;
        // compute the reduction, inductive case
        for ( size_t i = 0; i < aux.size(); ) {
            node::pointer_type l = aux[i].__level;    // current level
            size_t j;                                 // j will stop when the level is different

            // finds the end of the level
            //for ( j = i; j < aux.size() && level[sorted[j]] == l; ++j ) {
            for ( j = i; j < aux.size() && aux[j].__level == l; ++j ) {
                aux[j].setlo( reduced[aux[j].lo()] );
                aux[j].sethi( reduced[aux[j].hi()] );
            }

            // sort now lexicographically among nodes with the same level that is the interval
            // sorted[i, j)
            sort( aux.begin() + i, aux.begin() + j, lexsorter() );


            // iterates over the nodes of the current level, reducing them
            for ( ; i < j; ++i ) {
                node& current( aux[i].__node );

                if ( last == 0 || ( last > 0 && nodes[last] != current ) ) {
                    ++last;
                    nodes[last] = current;
                }

                reduced[aux[i].__inverse] = last - 1;
            }
        }

        //printf( "last part:      %lf s.\n", elapsed.elapsed() ); elapsed.restart();
        nodes.resize( last );

        // translate the roots
        // TODO
    }


    const size_t size( ) const {
        return nodes.size() - 2;
    }

    friend ostream& operator << ( ostream& out, const sharedsdd& s ) {
        for ( size_t i = 0; i < s.roots.size(); ++i )
            out << i << " => " << s.roots[i] << endl;
        out << endl;

        for ( size_t i = 0; i < s.nodes.size(); ++i )
            out << i << " => " << s.nodes[i] << endl;

        return out;
    }
};


#endif // SHAREDSDD_HPP
