#ifndef ZDD_HPP
#define ZDD_HPP

#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <boost/bimap.hpp>
#include <gmpxx.h>
#include <iterator>
#include <initializer_list>
#include "node.hpp"
using namespace std;
using namespace boost;

class zdd {
  bimap<size_t,node> __nodes;
  size_t             __root;

  typedef pair<size_t, size_t> memo_pair;
  typedef map<memo_pair, size_t> memo_type;
  typedef bimap<size_t, node> bm;
  typedef bimap<size_t, node>::value_type position;

  template<class OP>
  size_t meld_r ( size_t i, const zdd& g, size_t j, OP& op, bm& newnodes, memo_type& memo ) {
    size_t v =   __nodes.left.at( i ).val();
    size_t w = g.__nodes.left.at( j ).val();

    if ( v == node::value_max && w == node::value_max ) {
      // which sink is this?
      return op( __nodes.left.at( i ).lo(), g.__nodes.left.at( j ).lo() );
    }

    // the value of the melded node is always the smaller of the two
    size_t vv = v <= w ? v : w;

    // if v == w holds we proceed in both diagrams otherwise one stay at the
    // same position, and only the other proceeds
    size_t il = v <= w ? __nodes.left.at(i).lo() : i;
    size_t jl = v >= w ? g.__nodes.left.at(j).lo() : j;
    size_t ih = v <= w ? __nodes.left.at(i).hi() : 0;
    size_t jh = v >= w ? g.__nodes.left.at(j).hi() : 0;

    memo_pair lpair = {il, jl};
    memo_pair hpair = {ih, jh};
    size_t l, h;
    if ( memo.count( lpair ) == 0 ) {
      l = meld_r( il, g, jl, op, newnodes, memo );
      memo[lpair] = l;
    } else l = memo[lpair];
    if ( memo.count( hpair ) == 0 ) {
      h = meld_r( ih, g, jh, op, newnodes, memo );
      memo[hpair] = h;
    } else h = memo[hpair];

    if ( h != 0 ) {
      if ( newnodes.right.count( node( vv, l, h ) ) == 0 ) {
        newnodes.insert( position( newnodes.left.size(), node( vv, l, h ) ) );
        return newnodes.left.size() - 1;
      } else return newnodes.right.at( node( vv, l, h ) );
    } else return l;
  }

public:
  struct binary_or {
    inline bool operator() ( size_t a, size_t b ) {
      return (a == 1) || (b == 1);
    }
  };

  struct binary_and {
    inline bool operator() ( size_t a, size_t b ) {
      return (a == 1) && (b == 1);
    }
  };

  zdd( ) {
    __nodes.insert( position( 0, node( node::value_max, 0, 0 ) ) );
    __nodes.insert( position( 1, node( node::value_max, 1, 1 ) ) );
    __root = 0;
  }


  // I use a set here for two reasons:
  //  1) to avoid ambiguity in the case of {{1}, {2}, ..}
  //  2) because I want to sort the variables (maybe unnecessary)
  zdd( set<size_t>& s ) : zdd( ) {
    // the empty family has root pointing to top
    if ( s.size() == 0 ) {
      __root = 1;
      return;
    }

    set<size_t>::reverse_iterator it;
    for ( it = s.rbegin(); it != s.rend(); ++it )
      __nodes.insert( position( size() + 2, node( *it, 0, size() + 1 ) ) );

    __root = __nodes.left.size() - 1;
  }

  zdd( set<size_t>& s, const set<size_t>& specified, const size_t n ) : zdd( ) {

    for ( size_t i = n; i > 0; --i ) {
      if ( specified.count( i ) == 0 ) // unspecified
        __nodes.insert( position( size() + 2, node( i, size() + 1, size() + 1 ) ) );
        //__nodes.emplace_back( i, __nodes.size() - 1, __nodes.size() - 1 );
      else if ( s.count( i ) != 0 ) // specified in s
        __nodes.insert( position( size() + 2, node( i, 0, size() + 1 ) ) );
        //__nodes.emplace_back( i, 0, __nodes.size() - 1 );
      else {} // else suppression rule
        //__nodes.emplace_back( i, __nodes.size() - 1, 0 );
    }

    __root = size() + 1;
  }

  zdd( const initializer_list<initializer_list<size_t>>& sl ) : zdd() {
    typedef typename initializer_list<initializer_list<size_t>>::iterator iilit;
    for ( iilit i = sl.begin(); i != sl.end(); ++i ) {
      set<size_t> s( i->begin(), i->end() );
      zdd current( s );
      meld( current, binary_or() );
    }
  }

  zdd( const initializer_list<initializer_list<size_t>>& sl, const size_t n ) : zdd() {
    set<size_t> specified;
    typedef typename initializer_list<initializer_list<size_t>>::iterator iilit;

    // build a set of the specified variables
    for ( iilit i = sl.begin(); i != sl.end(); ++i )
      specified.insert( i->begin(), i->end() );

    // proceed as usual
    for ( iilit i = sl.begin(); i != sl.end(); ++i ) {
      set<size_t> s( i->begin(), i->end() );
      zdd current( s, specified, n );
      meld( current, binary_or() );
    }
  }

  size_t size() const {
    return __nodes.left.size() - 2;
  }

  template<class OP>
  zdd meld ( const zdd& g, OP op ) {
    memo_type memo;
    bm newnodes;
    newnodes.insert( position( 0, node( node::value_max, 0, 0 ) ) );
    newnodes.insert( position( 1, node( node::value_max, 1, 1 ) ) );

    __root = meld_r( __root, g, g.__root, op, newnodes, memo );
    __nodes = newnodes;
    return *this;
  }

  /*static size_t reduce_r ( const zdd& f, size_t i, map<node, size_t>& nodes, map<size_t, size_t>& memo, zdd& result ) {
    size_t v = f[i].val();
    size_t l = f[i].lo();
    size_t h = f[i].hi();

    if ( v == node::value_max ) return l;

    size_t newl, newh;
    if ( memo.count(l) == 0 ) {
      newl = reduce_r( f, l, nodes, memo, result );
      memo[l] = newl;
    } else newl = memo[l];
    if ( memo.count(h) == 0 ) {
      newh = reduce_r( f, h, nodes, memo, result );
      memo[h] = newh;
    } else newh = memo[h];

    node newone ( v, newl, newh );
    if ( nodes.count( newone ) == 0 ) {

      result.__nodes.emplace_back( newone );
      return nodes[newone] = nodes.size() - 1;
    } else return nodes[newone];
  }

  static zdd reduce ( const zdd& f ) {
    map<node, size_t> nodes;
    map<size_t, size_t> memo;
    nodes[f[0]] = 0;
    nodes[f[1]] = 1;

    zdd result;
    result.__root = reduce_r( f, f.__root, nodes, memo, result );
//    for (auto& p : nodes) {
//      cout << p.first << endl;
//    }
//    copy( buckets.begin(), buckets.end(), ostream_iterator<aux_node>(cout, "\n"));
    return result;
  }

//  template<class K, class R>
//  inline R memo_check ( K index, map<K, R>& memo ) {
//    R result;
//    if ( memo.count(index) == 0 ) {
//      result = count_r( index, memo );
//      memo[index] = result;
//    } else result = memo[index];

//    return result;
//  }*/

  mpz_class count_r ( const size_t i, map<size_t, mpz_class>& memo ) const {
    size_t l = __nodes.left.at(i).lo();
    size_t h = __nodes.left.at(i).hi();

    if ( i == 1 ) return 1;
    if ( i == 0 ) return 0;

    mpz_class cl, ch;
    if ( memo.count(l) == 0 ) {
      cl = count_r( l, memo );
      memo[l] = cl;
    } else cl = memo[l];
    if ( memo.count(h) == 0 ) {
      ch = count_r( h, memo );
      memo[h] = ch;
    } else ch = memo[h];

    return cl + ch;
  }

  mpz_class count ( ) const {
    map<size_t, mpz_class> memo;
    return count_r( __root, memo );
  }

  /*bool operator== ( const zdd& another ) const {
    if ( size() != another.size() ) return false;
    // start from the root and go down
  }

  const node operator[] ( size_t i ) const {
    return __nodes[i];
  }

  //http://reference.wolfram.com/mathematica/tutorial/GraphDrawing.html
  //http://reference.wolfram.com/mathematica/ref/LayeredGraphPlot.html
  ostream& mathematica ( ostream& out, bool omit_bottom_lo_edges = false ) const {
    size_t i;

    out << "{ ";
    out << "{ ";
    for ( i = 2; i < __nodes.size() - 1; ++i )
      if ( __nodes[i].lo() != 0 && omit_bottom_lo_edges )
        out << i << " -> " << __nodes[i].lo() << ", ";

    if ( __nodes[i].lo() != 0 && omit_bottom_lo_edges )
      out << i << " -> " << __nodes[i].lo();
    out << " }, ";

    out << "{ ";
    for ( i = 2; i < __nodes.size() - 1; ++i )
      out << i << " -> " << __nodes[i].hi() << ", ";

    out << i << " -> " << __nodes[i].hi();
    out << " }, ";

    out << "{ ";
    for ( i = 0; i < __nodes.size() - 1; ++i )
      out << (__nodes[i].val() == node::value_max ? i : __nodes[i].val()) << ", ";

    out << __nodes[i].val();
    out << " }";
    out << " }";

    return out;
  }*/

  friend ostream& operator << ( ostream& out, const zdd& z ) {
    out << "root => " << z.__root << endl;
    for ( auto i : z.__nodes.left )
      out << i.first << " => " << i.second << endl;

    return out;
  }
};


#endif // ZDD_HPP
