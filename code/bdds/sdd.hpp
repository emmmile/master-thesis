#ifndef SDD_HPP
#define SDD_HPP

#include <string>
#include <vector>
#include "zdd.hpp"
//#include "sddbase.hpp"
class sddbase;

class sdd : public zdd {
    friend class sddbase;

public:
    sdd() : zdd() {
    }

    static sdd allsubstrings ( const string& s ) {
        sdd out;
        int i = s.size() - 1;
        out.__nodes.emplace_back( s[i], 0, 1 );
        out.__nodes.emplace_back( s[i], 1, 1 );
        --i;
        int j = 2;

        for ( ; i > 0; --i ) {
            out.__nodes.emplace_back( s[i], out.__nodes[j].lo() + 2, out.__nodes[j].hi() + 2 ); ++j;
            out.__nodes.emplace_back( s[i], 1, out.__nodes[j].hi() + 2 ); ++j;
        }



        out.__nodes.emplace_back( s[0], out.__nodes.size() - 2, out.__nodes.size() - 1 );
        out.__root = out.__nodes.size() - 1;
        cout << out << endl;
        getchar();

        out.reduce();
        return out;
    }

    sdd( const zdd& z ) : zdd( z ) {
    }

    sdd ( const string& s ) : sdd( s.c_str() ) { }

    sdd( const char* s ) : sdd() {
        size_t len = strlen( s );
        if ( len == 0 ) {
            this->__root = 1;
            return;
        }

        this->__nodes.reserve( len + 2 );

        for ( int i = len - 1; i >= 0; --i )
            this->__nodes.emplace_back( s[i], 0, this->__nodes.size() - 1 );

        this->__root = this->__nodes.size() - 1;
        //this->__max = this->__nodes[2].val();
    }

    // http://stackoverflow.com/questions/1059630/default-value-to-a-parameter-while-passing-by-reference-in-c
    template<class Container, class Allocator = zallocator>
    sdd( const Container& sl, const Allocator& m = Allocator() ) : sdd() {
        for ( auto s : sl ) {
            sdd current( s );
            *this = meld( *this, current, binary_or(), m );
        }
    }

    template<class Allocator = zallocator>
    sdd( const initializer_list<const char*>& sl, const Allocator& m = Allocator() ) : sdd() {
        for ( auto s : sl ) {
            sdd current( s );
            *this = meld( *this, current, binary_or(), m );
        }
    }

    friend ostream& operator << ( ostream& out, const sdd& z ) {
        size_t i;
        out << "root => " << z.__root << endl;
        //out << "min => " << z.__nodes[z.__root].val() << endl;
        //out << "max => " << z.__max << endl;
        for ( i = 0; i < z.__nodes.size(); ++i )
            if ( i <= 1 )
                out << i << " => (" << int(z.__nodes[i].val()) << ", " << z.__nodes[i].lo() << ", " << z.__nodes[i].hi() << ")\n";
            else
                out << i << " => (" << z.__nodes[i].val() << ", " << z.__nodes[i].lo() << ", " << z.__nodes[i].hi() << ")\n";

        return out;
    }
};

#endif // SDD_HPP
