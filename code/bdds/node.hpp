#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>
#include <limits>
#include <functional>
#include <inttypes.h>
using namespace std;

struct binary_or {
    inline bool operator() ( size_t a, size_t b ) {
        return (a == 1) || (b == 1);
    }
};

struct binary_and {
    inline bool operator() ( size_t a, size_t b ) {
        return (a == 1) && (b == 1);
    }
};


#define USE_KNUTH 1

#if !USE_KNUTH
class node {
protected:

    int32_t __data[3];
public:
    typedef int32_t value_type;
    typedef int32_t pointer_type;
    static const value_type value_max = numeric_limits<value_type>::max();
    static const pointer_type pointer_max     = std::numeric_limits<pointer_type>::max();

    node ( ) : node( 0,0,0 ) { }

    node ( size_t v, size_t l, size_t h ) {
        __data[0] = v;
        __data[1] = l;
        __data[2] = h;
    }

    size_t hash ( ) const {
        size_t hash = 0;
        for ( size_t i = 0; i < 3; ++i )
            hash = (hash+(324723947+__data[i]))^93485734985;
        return hash;

        //    size_t seed = std::hash<size_t>()(__data[1]);
        //    seed = std::hash<size_t>()(__data[2]) + 0x9e3779b9 + (seed<<6) + (seed>>2);

        //    return (seed & ~(255 << 24)) | __data[0] << 24;
    }

    int32_t val ( ) const {
        return __data[0];
    }

    int32_t lo ( ) const {
        return __data[1];
    }

    int32_t hi ( ) const {
        return __data[2];
    }

    void get ( value_type& v, pointer_type& l, pointer_type& h ) {
        v = val();
        l = lo();
        h = hi();
    }

    void setval ( value_type v ) {
        __data[0] = v;
    }

    void setlo ( pointer_type v ) {
        __data[1] = v;
    }

    void sethi ( pointer_type v ) {
        __data[2] = v;
    }

    bool operator> ( const node& another ) const {
        return val() > another.val() ? true : (lo() > another.lo() ? true : hi() > another.hi());
    }

    bool operator< ( const node& another ) const {
        return val() < another.val() ? true : (lo() < another.lo() ? true : hi() < another.hi());
    }

    bool operator== ( const node& another ) const {
        return val() == another.val() && lo() == another.lo() && hi() == another.hi();
    }

    bool operator!= ( const node& another ) const {
        return !( *this == another );
    }

    friend ostream& operator << ( ostream& out, const node& n ) {
        return out << "(" << n.val() << ", " << n.lo() << ", " << n.hi() << ")";
    }
};

#else

class node {
public:
    typedef uint64_t        node_type; // unsigned __int128
    typedef unsigned char   value_type;
    typedef uint32_t        pointer_type;

    static const size_t       value_width     = 8;
    static const size_t       pointer_width   = ( sizeof( node_type ) * 8 - value_width ) / 2;
    static const node_type    value_mask      = ((1 << value_width) - 1);
    static const node_type    pointer_mask    = ((1 << pointer_width) - 1);
    static const value_type   value_max       = std::numeric_limits<value_type>::max();
    static const pointer_type pointer_max     = std::numeric_limits<pointer_type>::max();

    node_type __node;

    node ( ) : __node( 0 ) { }

    node ( value_type v, pointer_type l, pointer_type h ) : __node( 0 ) {
        setval( v );
        setlo( l );
        sethi( h );
    }

    value_type val ( ) const {
        return __node >> (pointer_width + pointer_width);
    }

    pointer_type lo ( ) const {
        return (__node & (pointer_mask << pointer_width)) >> pointer_width;
    }

    pointer_type hi ( ) const {
        return __node & pointer_mask;
    }

    void get ( value_type& v, pointer_type& l, pointer_type& h ) {
        v = val();
        l = lo();
        h = hi();
    }

    void setval ( value_type v ) {
        __node &= ~(value_mask << (pointer_width + pointer_width));
        __node |= (v & value_mask) << (pointer_width + pointer_width);
    }

    void setlo ( pointer_type v ) {
        __node &= ~(pointer_mask << pointer_width);
        __node |= (v & pointer_mask) << pointer_width;
    }

    void sethi ( pointer_type v ) {
        __node &= ~pointer_mask;
        __node |= v & pointer_mask;
    }

    inline node_type hash () const {
        return __node;
    }

    bool operator< ( const node& another ) const {
        return __node < another.__node;
    }

    bool operator> ( const node& another ) const {
        return __node > another.__node;
    }

    bool operator== ( const node& another ) const {
        return __node == another.__node;
    }

    bool operator!= ( const node& another ) const {
        return __node != another.__node;
    }

    friend ostream& operator << ( ostream& out, const node& n ) {
        return out << "(" << char(n.val()) << ", " << n.lo() << ", " << n.hi() << ")";
    }
};

#endif

const node::value_type node::value_max;


namespace std {
template<>
class hash<node> {
public:
    inline size_t operator() ( const node& n ) const {
        return n.hash();
    }
};

template<class T>
class hash<pair<T, T>> {
private:
    const hash<T> ah;
public:
    inline size_t operator() ( const pair<T, T>& p ) const {
        // the boost way
        size_t seed = ah(p.first);
        return ah(p.second) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    }
};
}


#endif // NODE_HPP
