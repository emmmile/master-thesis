#ifndef SDDBASE_HPP
#define SDDBASE_HPP

#include <unordered_set>
#include <boost/bimap.hpp>
#include <boost/bimap/unordered_set_of.hpp>
#include <boost/bimap/set_of.hpp>
#include "zdd.hpp"
#include "sdd.hpp"

#include <google/sparse_hash_map>
#include <chrono>
using namespace std;
using namespace boost;
using namespace boost::bimaps;


template<class T, class V, class THasher = std::hash<T>>
class googlehash : public google::sparse_hash_map<T, V, THasher> {
public:
    const V at ( const T& k ) const {
        return this->find(k)->second;
    }
};

template<class T, class V, class THasher = std::hash<T>, class VHasher = std::hash<V>>
class doublemap {
public:
    googlehash<T, V, THasher> left;
    googlehash<V, T, VHasher> right;

    typedef pair<V, T> right_type;
    typedef pair<T, V> left_type;
    typedef left_type value_type;

    void insert( const value_type& v ) {
        left.insert( v );

        right_type vv( v.second, v.first );
        right.insert( vv );
    }

    size_t size ( ) const {
        return left.size();
    }
};

#define COUNTER_HASH 0

class sddbase {
    typedef bimap<unordered_set_of<node, std::hash<node> >, unordered_set_of<node::pointer_type> > sddmap;
    //typedef doublemap<node, node::pointer_type> sddmap;
    typedef typename sddmap::value_type spair;


    typedef bignum                   big_type; // mpz_class does not respect allocator specification
    sddmap                              __unique;
    node::pointer_type                  __fresh;
    vector<node::pointer_type>          roots;
    friend class sdd;

#if !COUNTER_HASH
    vector<big_type> __counter;
#else
    unordered_map<node::pointer_type, big_type> __counter;
#endif

    ///
    /// \brief add_r
    /// \param i the index inside the sdd
    /// \param s the sdd to insert
    /// \return an sdd node representing the inserted (so far) sequence
    ///
    node insert_r ( const sdd& s, const node::pointer_type i ) {
        node::pointer_type l = s[i].lo();
        node::pointer_type h = s[i].hi();
        node::value_type v = s[i].val();

        // if I found a sink, return it (it will be surely contained in the base)
        if ( v == node::value_max ) return s[i];

        // recursively insert the childrens
        node newl = insert_r( s, l );
        node newh = insert_r( s, h );

        // insert the current node, having actual nodes for the childrens
        node current( v, __unique.left.at(newl), __unique.left.at(newh) );
        if ( __unique.left.count( current ) == 0 )
            insert( current );

        return current;
    }

    template<class Allocator>
    void count_r ( const node::pointer_type i, zmap<node::pointer_type, big_type, Allocator>& memo ) const {
        node::pointer_type l = __unique.right.at( i ).lo();
        node::pointer_type h = __unique.right.at( i ).hi();

        if ( i == 1 ) { memo[1] = 1; return; }
        if ( i == 0 ) { memo[0] = 0; return; }

        if ( memo.count(l) == 0 ) count_r( l, memo );
        if ( memo.count(h) == 0 ) count_r( h, memo );

        memo[i] = memo[l] + memo[h];
    }

public:
    ///
    /// \brief Builds an empty sdd base, containing only the two sinks.
    ///
    sddbase() {
        __unique.insert( spair( node( node::value_max, 0, 0 ), 0 ) );
        __unique.insert( spair( node( node::value_max, 1, 1 ), 1 ) );
        #if COUNTER_HASH
        __counter[0] = 0;
        __counter[1] = 1;
        #else
        __counter.push_back( 0 );
        __counter.push_back( 1 );
        #endif
    }


    size_t hashsize( ) const {
        return __unique.left.bucket_count();
    }


    inline node::pointer_type insert ( const node& n ) {
        node::pointer_type newone = fresh();
        __unique.insert( spair( n, newone ) );
#if COUNTER_HASH
        __counter[newone] = __counter[n.lo()] + __counter[n.hi()];
#else
        __counter.push_back( __counter[n.lo()] + __counter[n.hi() ] );
#endif
        return newone;
    }

    node allsubstrings ( const string& s ) {
        int i = s.size() - 1;
        //printf( "Inserting \"%c\" at %d\n", s[i], fresh() );
        insert( node( s[i], 0, 1 ) );
        insert( node( s[i], 1, 1 ) );
        roots.push_back( fresh()-2 );

        if ( s.size() == 1 ) return __unique.right.at( fresh()-1 );

        for ( --i; i > 0; --i ) {
            //printf( "Inserting \"%c\" at %d\n", s[i], fresh() );
            insert( node( s[i], 0, __unique.right.at(fresh()-2).hi() + 2 ) );
            insert( node( s[i], 1, __unique.right.at(fresh()-2).hi() + 2 ) );
            roots.push_back( fresh()-2 );
        }

        //printf( "Inserting \"%c\" at %d\n", s[i], fresh() );
        insert( node( s[0], 0, fresh()-1 ) );
        roots.push_back( fresh()-1 );


        return __unique.right.at( fresh()-1 );
    }

    node computeallsubstrings ( ) {
        node result = __unique.right.at( roots.back() );

        for ( int i = roots.size() - 1; i >= 0; i-- ) {
        //for ( int i =0; i < roots.size(); ++i ) {
            //cout << i << endl;
            node suffix = __unique.right.at( roots[i] );
            result = meld( suffix, result, binary_or() );
        }

        return result;
    }

    /// TODO this algorithm can also be used to convert a sddbase to a sdd
    node build_r ( const sddbase& another, const node root ) {
        node::pointer_type l = root.lo();
        node::pointer_type h = root.hi();
        node::value_type v = root.val();

        // if I found a sink, return it (it will be surely contained in the base)
        if ( v == node::value_max ) return root;

        // recursively insert the childrens
        node newl = build_r( another, another.__unique.right.at(l) );
        node newh = build_r( another, another.__unique.right.at(h) );

        // insert the current node, having actual nodes for the childrens
        node current( v, __unique.left.at(newl), __unique.left.at(newh) );
        if ( __unique.left.count( current ) == 0 )
            insert( current );

        return current;
    }

    sddbase( const sddbase& another, const node root, node& newroot ) : sddbase() {
        newroot = build_r( another, root );
    }

    ///
    /// \return The sdd representing the empty family
    node empty ( ) const {
        return __unique.right.at( 0 );
    }

    node insert ( const char* s ) {
        return insert( sdd( s ) );
    }

    template<class S>
    node insert ( const S& s ) {
        return insert( s.c_str() );
    }

    node insert ( const sdd& s ) {
        return insert_r( s, s.root() );
    }

    template <class InputIterator>
    void insert ( InputIterator first, InputIterator last ) {
        for ( ; first != last; ++first ) insert( *first );
    }

    const size_t size( ) const {
        return __unique.size() - 2;
    }

    inline node::pointer_type fresh( ) {
        return __unique.size();
        //return ++__fresh - 1;
    }

    void size_r ( const node::pointer_type i, unordered_set<node::pointer_type>& reachable ) const {
        node::pointer_type l = __unique.right.at( i ).lo();
        node::pointer_type h = __unique.right.at( i ).hi();

        reachable.insert( i );
        if ( i == 0 || i == 1 ) return;
        if ( reachable.count( l ) == 0 ) size_r( l, reachable );
        if ( reachable.count( h ) == 0 ) size_r( h, reachable );
    }

    // returns the number of nodes reachable from f
    const size_t size ( const node f ) const {
        unordered_set<node::pointer_type> reachable;
        size_r( __unique.left.at( f ), reachable );
        return reachable.size();
    }

    friend ostream& operator << ( ostream& out, const sddbase& s ) {

        for ( size_t i = 0; i < s.roots.size(); ++i )
            out << i << " => " << s.roots[i] << endl;

        for ( auto i : s.__unique.right )
            out << i.first << " => " << i.second << endl;

        return out;
    }

    template<class OP, class Allocator = zallocator>
    node::pointer_type meld_r ( const node::pointer_type i, const node::pointer_type j, OP op, zmap<zpair, node::pointer_type, Allocator>& memo ) {
        node f = __unique.right.at( i );
        node g = __unique.right.at( j );
        node current;

        if ( i == 0 || j == 0 || g == f ) {
            if ( i != 0 ) return i;
            else if ( j != 0 ) return j;
            else return op( i, j );
        } else if ( memo.count( {i, j} ) != 0 ) {
            return memo.at( {i, j} );
        } else {
            if ( j == 1 || f.val() < g.val() )
                current = node( f.val(), meld_r( f.lo(), j, op, memo ),
                                meld_r( f.hi(), 0, op, memo ) );
            else if ( i == 1 || f.val() > g.val() )
                current = node( g.val(), meld_r( i, g.lo(), op, memo ),
                                meld_r( 0, g.hi(), op, memo ) );
            else if ( f.val() == g.val() )
                current = node( g.val(), meld_r( f.lo(), g.lo(), op, memo ),
                                meld_r( f.hi(), g.hi(), op, memo ) );


            // if node does not exist in the base
            if ( __unique.left.count( current ) == 0 ) {
                // effectively add it
                return insert( current );
                // otherwise return the index of the existing node
            } else
                return __unique.left.at( current );
        }
    }

    template<class OP, class Allocator = zallocator>
    node meld ( const node f, const node g, OP op ) {
        zmap<zpair, node::pointer_type, Allocator> memo;
        node::pointer_type root = meld_r( __unique.left.at( f ), __unique.left.at( g ), op, memo );
        return __unique.right.at( root );
    }


    void random ( const node& current ) const {
        node f = current;
        size_t counter = 0;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 generator ( seed );

        while ( f.val() != node::value_max ) {
            ++counter;
            node::pointer_type l = f.lo();
            node::pointer_type h = f.hi();

            big_type total = __counter[__unique.left.at( f )];
            big_type pivot = __counter[l];
            big_type toss  = big_type::rand( total, generator );

            //cout << pivot << " " << total << " " << toss << endl;
            //getchar();

            if ( toss < pivot ) {
                f = __unique.right.at( l );
            } else {
                cout << (char) f.val();
                f = __unique.right.at( h );
            }
        }

        cout << endl;
    }


    //template<class Allocator = zallocator>
    big_type count ( const node& f ) const {
        //zmap<node::pointer_type, big_type, Allocator> memo;
        //count_r( __unique.left.at( f ), memo );
        //return memo[__unique.left.at( f )];
        #if COUNTER_HASH
        return __counter.find( __unique.left.at( f ) )->second;
        #else
        return __counter[ __unique.left.at( f ) ];
        #endif
    }
};

#endif // SDDBASE_HPP
