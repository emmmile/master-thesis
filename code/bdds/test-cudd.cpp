


#include <cudd/cuddObj.hh>
#include <iostream>
using namespace std;


int main () {
    Cudd mgr(0,0);
    ZDD x = mgr.zddVar(1);
    ZDD y = mgr.zddVar(1);
    ZDD f = x * y;
    ZDD g = y + x;
    cout << "f is" << (f <= g ? "" : " not")
         << " less than or equal to g\n";
}
