
#include <boost/timer/timer.hpp>
#include <fstream>
#include <iomanip>  // std::setprecision
#include <iostream>
#include <set>
#include <streambuf>
#include <string>

#include "zdd.hpp"
using namespace std;

int main() {
  //  set<node> nodes;
  //  for ( int i = 0; i < 10000; ++i )
  //    nodes.insert( node(random() % 256, random() % 1000, random() % 1000) );

  //  zdd z = {{1,3,5}, {1,4}};
  //  zdd y = {{2,4,6}, {2,5}, {3,6}};
  //  zdd w = zdd::meld( y, z, binary_or() );
  //  cout << w << endl;
  //  cout << w.reduce_knuth() << endl;
  // stackallocator<int> m;
  // std::allocator<int> m;

  size_t n = 128;
  zdd is({{}}, n);
  for (size_t i = 1; i <= n; ++i) {
    size_t u = i;
    size_t v = i % n + 1;
    zdd current({{}, {u}, {v}}, n);
    is = zdd::meld(is, current, binary_and());
  }

  cout << endl
       << "IS ZDD size:     \t" << fixed << setprecision(0) << is.size()
       << endl;
  cout << "Total solutions:\t" << is.count() << endl;

  zdd ds({{}}, n);
  for (size_t i = 1; i <= n; ++i) {
    size_t u = i == 1 ? n : i - 1;
    size_t v = i % n + 1;
    zdd current({{i}, {u}, {v}, {i, u}, {i, v}, {u, v}, {i, u, v}}, n);
    ds = zdd::meld(ds, current, binary_and());
  }

  cout << endl << "DS ZDD size:        \t" << ds.size() << endl;
  cout << "Total solutions:\t" << ds.count() << endl;

  // boost::progress_timer t;
  // zdd::debugmeld( is, ds );
  // cout << t.elapsed() << endl;
  zdd kernels = zdd::meld(is, ds, binary_and());

  cout << endl << "Kernels ZDD size:    \t" << kernels.size() << endl;
  cout << "Total solutions:\t" << kernels.count() << endl;

  ofstream outfile("data/kernels");
  kernels.mathematica(outfile, true);

  // zdd w( {{1,2,3}, {4}, {5,6}}, 1, 6 encoding );
  // cioe' descrivo
  // 1) quello che voglio rappresentare
  // 2) l'universo, o tramite min-max oppure proprio dando la sua totale
  // rappresentazione 3) una funzione che mappa elementi di U in N.

  // si potrebbe definire anche il concetto di variabile V
  // il tipo dell'universo e' U
  // encoding e' una funzione invertibile da U a V
  // ordering e' una permutazione di V, cioe' una funzione invertibile da V a V
}
