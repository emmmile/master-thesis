#ifndef ZDD_HPP
#define ZDD_HPP

#include <algorithm>
#include <cassert>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "node.hpp"
#include "utils/utils.hpp"
#include <boost/multiprecision/cpp_int.hpp>
using namespace boost::multiprecision;
using namespace std;

// types for convenience in constructors
template <class Unsigned>
using nested_list = initializer_list<initializer_list<Unsigned>>;

template <class Key, class Value>
using zmap =
    class unordered_map<Key, Value, std::hash<Key>, std::equal_to<Key>>;

template <class Key>
using zset = class unordered_map<Key, std::hash<Key>, std::equal_to<Key>>;

typedef pair<node::pointer_type, node::pointer_type> zpair;

// typedef std::allocator<size_t> zallocator;

#define LINEAR_REDUCE 0

class entry {
 public:
  node __node;
  node::pointer_type __level;
  node::pointer_type __inverse;

  entry() {}
  entry(const node& node, node::pointer_type l, node::pointer_type i)
      : __node(node), __level(l), __inverse(i) {}

  inline void setlo(node::pointer_type v) { __node.setlo(v); }
  inline void sethi(node::pointer_type v) { __node.sethi(v); }
  inline node::pointer_type lo() { return __node.lo(); }
  inline node::pointer_type hi() { return __node.hi(); }
};

struct levelsorter {
  inline bool operator()(const entry& a, const entry& b) const {
    return (a.__level < b.__level);
  }
};

struct lexsorter {
  inline bool operator()(const entry& a, const entry& b) {
    return a.__node < b.__node;
  }
};

class zdd {
 protected:
  vector<node> __nodes;  // or array?
  node::pointer_type __root;
  // node::value_type         __max;

  typedef cpp_int big_type;

  template <class OP, class map_type>
  static node::pointer_type meld_r(const zdd& f, node::pointer_type i,
                                   const zdd& g, node::pointer_type j,
                                   vector<node>& newnodes, OP& op,
                                   map_type& memo) {
    node::value_type v = f[i].val();
    node::value_type w = g[j].val();

    if ((v == node::value_max) && (w == node::value_max)) {
      // which sink is this?
      return op(f[i].lo(), g[j].lo());
    }

    // the value of the melded node is always the smaller of the two
    node::value_type vv = v <= w ? v : w;

    // if v == w holds we proceed in both diagrams otherwise one stay at the
    // same position, and only the other proceeds
    node::pointer_type il = v <= w ? f[i].lo() : i;
    node::pointer_type jl = v >= w ? g[j].lo() : j;
    node::pointer_type ih = v <= w ? f[i].hi() : 0;
    node::pointer_type jh = v >= w ? g[j].hi() : 0;

    zpair lpair = {il, jl};
    zpair hpair = {ih, jh};
    node::pointer_type l, h;

    bool ltrivial = (il == 0 || il == 1) && (jl == 0 || jl == 1);
    bool htrivial = (ih == 0 || ih == 1) && (jh == 0 || jh == 1);
    if (ltrivial) l = op(il, jl);
    if (htrivial) h = op(ih, jh);

    if (!ltrivial) {
      // printf( "%zu, %zu\n", il, jl );
      if (memo.count(lpair) == 0) {
        l = meld_r(f, il, g, jl, newnodes, op, memo);
        memo[lpair] = l;
      } else
        l = memo[lpair];
    }
    if (!htrivial) {
      // printf( "%zu, %zu\n", ih, jh );
      if (!htrivial && memo.count(hpair) == 0) {
        h = meld_r(f, ih, g, jh, newnodes, op, memo);
        memo[hpair] = h;
      } else
        h = memo[hpair];
    }

    if (h != 0) {
      newnodes.emplace_back(vv, l, h);
      return newnodes.size() - 1;
    } else
      return l;
  }

  template <class map_type>
  big_type count_r(const node::pointer_type i, map_type& memo) const {
    node::pointer_type l = __nodes[i].lo();
    node::pointer_type h = __nodes[i].hi();

    if (i == 1) return 1;
    if (i == 0) return 0;

    big_type cl, ch;
    if (memo.count(l) == 0) {
      cl = count_r(l, memo);
      memo[l] = cl;
    } else
      cl = memo[l];
    if (memo.count(h) == 0) {
      ch = count_r(h, memo);
      memo[h] = ch;
    } else
      ch = memo[h];

    return cl + ch;
  }

  // I use a sorted unique range because:
  //  1) to avoid ambiguity in the case of {{1}, {2}, ..}
  //  2) because I want to sort the variables (maybe unnecessary)
  template <class random_iterator>
  zdd(random_iterator beg, random_iterator end) : zdd() {
    // the empty family has root pointing to top
    if (end - beg == 0) {
      __root = 1;
      return;
    }

    // don't know if this is correct but is working for the moment
    // it emulates reverse iterating
    random_iterator it;
    for (it = end - 1; it != beg - 1; --it)
      __nodes.emplace_back(*it, 0, __nodes.size() - 1);

    __root = __nodes.size() - 1;
    //__max  = *(end - 1);
  }

  template <class random_iterator>
  zdd(random_iterator cbeg, random_iterator cend, random_iterator sbeg,
      random_iterator send, const size_t n)
      : zdd() {
    for (size_t i = n; i > 0; --i) {
      if (binary_search(sbeg, send, i) == false)  // unspecified
        __nodes.emplace_back(i, __nodes.size() - 1, __nodes.size() - 1);
      else if (binary_search(cbeg, cend, i) ==
               true)  // specified in the current vars
        __nodes.emplace_back(i, 0, __nodes.size() - 1);
      else {
      }  // else suppression rule
    }

    __root = __nodes.size() - 1;
    //__max = *(cend - 1);
  }

 public:
  // CONSTRUCTORS
  ///
  /// \brief zdd
  /// Just builds an empty diagram, representing the empty family
  zdd() {
    __nodes.reserve(4);
    __nodes.emplace_back(node::value_max, 0, 0);
    __nodes.emplace_back(node::value_max, 1, 1);
    __root = 0;
    //        //__max  = 0;

    //        __nodes.emplace_back( node::value_max, 0, 0 );
    //        __nodes.emplace_back( node::value_max, 1, 1 );
    //        __nodes.emplace_back( 3, 0, 1 );
    //        __nodes.emplace_back( 2, 2, 6 );
    //        __nodes.emplace_back( 1, 3, 5 );
    //        __nodes.emplace_back( 3, 0, 1 );
    //        __nodes.emplace_back( 3, 0, 1 );
    //        __root = 4;
  }

  ///
  /// Builds the zdd from a list of initilizer lists.
  /// The specified items will be the only items in the family.
  /// The allocator is specified in order to easily use a custom one.
  ///
  zdd(const nested_list<size_t>& sl) : zdd() {
    typedef typename nested_list<size_t>::iterator nlit;

    for (nlit i = sl.begin(); i != sl.end(); ++i) {
      size_t vcurrent[i->size()];
      copy(i->begin(), i->end(), vcurrent);
      size_t vsize = sort_unique(vcurrent, vcurrent + i->size());

      zdd current(vcurrent, vcurrent + vsize);
      *this = meld(*this, current, binary_or());
    }

    // melding also reduces
    // reduce();
  }

  ///
  /// Builds the zdd considering a set of free variables.
  /// The specified items will be part of the family, together with ANY
  /// combination of the unspecified ones. This is useful to express boolean
  /// formulas where often the unspecified variables have this meaning.
  ///
  zdd(const nested_list<size_t>& sl, const size_t n) : zdd() {
    typedef typename nested_list<size_t>::iterator nlit;
    // this actually is not safe, but is very rare that the user will specify
    // all the variables
    size_t specified[n];
    // size_t specified [n * sl.size()];

    size_t total = 0;
    // build a set of the specified variables
    for (nlit i = sl.begin(); i != sl.end(); ++i) {
      copy(i->begin(), i->end(), specified + total);
      total += i->size();
    }

    size_t ssize = sort_unique(specified, specified + total);

    // proceed as usual
    for (nlit i = sl.begin(); i != sl.end(); ++i) {
      size_t vcurrent[i->size()];
      copy(i->begin(), i->end(), vcurrent);
      size_t vsize = sort_unique(vcurrent, vcurrent + i->size());

      zdd current(vcurrent, vcurrent + vsize, specified, specified + ssize, n);
      *this = meld(current, *this, binary_or());
    }

    // melding also reduces
    // reduce();
    // assert( zdd::reduce_naive( *this ).size() == reduce().size() );
  }

  ///
  /// \return the size of the diagram, the number of its nodes (except the
  /// sinks)
  ///
  size_t size() const { return __nodes.size() - 2; }

  ///
  /// \return true if the two diagrams are equal, i.e. represents the same
  /// family
  ///
  bool operator==(const zdd& another) const {
    if (size() != another.size()) return false;
    // TODO check the tree!
  }

  ///
  /// returns the i-th node
  ///
  const node& operator[](node::pointer_type i) const { return __nodes[i]; }

  ///
  /// \return a pointer the root node (it will be an integral tipe)
  ///
  const node::pointer_type root() const { return __root; }

#if LINEAR_REDUCE
  node::pointer_type compute_levels_tail(const node::pointer_type i,
                                         vector<entry>& aux) const {
    if (aux[i].__level != 0) {
      return aux[i].__level;
    }
    if (__nodes[i].val() == node::value_max) {
      return aux[i].__level = 0;
    }

    return aux[i].__level = max(compute_levels_tail(__nodes[i].lo(), aux),
                                compute_levels_tail(__nodes[i].hi(), aux)) +
                            1;
  }

  void reachable(const node::pointer_type i, vector<entry>& aux) const {
    aux[i].__level = 1;
    if (__nodes[i].val() == node::value_max) return;

    node::pointer_type l = __nodes[i].lo();
    node::pointer_type h = __nodes[i].hi();
    if (aux[l].__level != 1) reachable(l, aux);
    if (aux[h].__level != 1) reachable(h, aux);
  }

  ///
  /// \brief reduce
  /// \return
  ///
  zdd& reduce(bool verbose = false) {
    vector<node::pointer_type> reduced(__nodes.size());
    vector<entry> aux(__nodes.size());

    // disconnect nodes creating redundancy
    for (size_t i = 0; i < __nodes.size(); ++i) {
      if (__nodes[__nodes[i].lo()].val() == __nodes[i].val() &&
          __nodes[i].val() != node::value_max) {
        cout << "unlinking node " << i << endl;
        __nodes[i].setlo(0);
      }

      aux[i].__level = 0;
    }

    // search for unreachable nodes
    reachable(__root, aux);
    // now all reachable nodes are marked with level == 1
    for (size_t i = 0; i < __nodes.size(); ++i) {
      // XXX temporary hack, replace the non reachable nodes with a 0-sink
      if (aux[i].__level == 0) __nodes[i] = {node::value_max, 0, 0};

      // initialize auxiliary array
      reduced[i] = node::pointer_max;
      aux[i] = {__nodes[i], 0, (node::pointer_type)i};
    }

    compute_levels_tail(__root, aux);
    sort(aux.begin(), aux.end(), levelsorter());

    fill(__nodes.begin() + 1, __nodes.end(), node());

    reduced[0] = 0;
    reduced[1] = 1;
    size_t last = 0;
    // compute the reduction, inductive case
    for (size_t i = 0; i < aux.size();) {
      node::pointer_type l = aux[i].__level;  // current level
      size_t j;  // j will stop when the level is different

      // finds the end of the level
      // for ( j = i; j < aux.size() && level[sorted[j]] == l; ++j ) {
      for (j = i; j < aux.size() && aux[j].__level == l; ++j) {
        aux[j].setlo(reduced[aux[j].lo()]);
        aux[j].sethi(reduced[aux[j].hi()]);
      }

      // sort now lexicographically among nodes with the same level that is the
      // interval sorted[i, j)
      sort(aux.begin() + i, aux.begin() + j, lexsorter());

      // iterates over the nodes of the current level, reducing them
      for (; i < j; ++i) {
        node& current(aux[i].__node);

        if (__nodes[last] != current) {
          // if ( __nodes[last] != current ) {
          ++last;
          __nodes[last] = current;
        }

        reduced[aux[i].__inverse] = last;
      }
    }

    // printf( "last part:      %lf s.\n", elapsed.elapsed() );
    // elapsed.restart();
    __nodes.resize(last + 1);
    __root = reduced[__root];
    return *this;
  }

#else
  static size_t reduce_r(const zdd& f, size_t i, zmap<node, size_t>& nodes,
                         vector<size_t>& memo, zdd& result) {
    node::value_type v = f[i].val();
    node::pointer_type l = f[i].lo();
    node::pointer_type h = f[i].hi();

    if (v == node::value_max) return l;

    node::pointer_type newl, newh;
    if (memo[l] == size_t(-1)) {
      newl = reduce_r(f, l, nodes, memo, result);
      memo[l] = newl;
    } else
      newl = memo[l];
    if (memo[h] == size_t(-1)) {
      newh = reduce_r(f, h, nodes, memo, result);
      memo[h] = newh;
    } else
      newh = memo[h];

    node newone(v, newl, newh);
    if (nodes.count(newone) == 0) {
      result.__nodes.emplace_back(newone);
      return nodes[newone] = nodes.size() - 1;
    } else
      return nodes[newone];
  }

  zdd& reduce(bool verbose = false) {
    zmap<node, size_t> nodes(size() + 2, std::hash<node>(),
                             std::equal_to<node>());
    vector<size_t> aux(size() + 2, -1);
    nodes[__nodes[0]] = 0;
    nodes[__nodes[1]] = 1;

    zdd result;
    result.__root = reduce_r(*this, __root, nodes, aux, result);
    *this = result;
    return *this;
  }

#endif

  big_type count() const {
    zmap<node::pointer_type, big_type> memo(
        size() + 2, std::hash<node::pointer_type>(),
        std::equal_to<node::pointer_type>());
    return count_r(__root, memo);
  }

  template <class OP>
  static zdd meld(const zdd& f, const zdd& g, OP op) {
    zdd out;
    // hint for a good size
    zmap<zpair, node::pointer_type> memo(1.5 * (f.size() + g.size() + 2),
                                         std::hash<zpair>(),
                                         std::equal_to<zpair>());

    // cout << int(f.__max) << " " << int(g.__max) << endl;
    out.__root = meld_r(f, f.__root, g, g.__root, out.__nodes, op, memo);
    // out.__max = max( f.__max, g.__max );
    // cout << int(out.__max) << endl;
    // cout << out << endl;

    return out.reduce(false);
  }

  // OUTPUT
  // http://reference.wolfram.com/mathematica/tutorial/GraphDrawing.html
  // http://reference.wolfram.com/mathematica/ref/LayeredGraphPlot.html
  template <class T = int>
  ostream& mathematica(ostream& out, bool omit_bottom_lo_edges = false) const {
    size_t i;

    out << "{ ";
    out << "{ ";
    for (i = 2; i < __nodes.size() - 1; ++i)
      if (__nodes[i].lo() != 0) out << i << " -> " << __nodes[i].lo() << ", ";

    if (__nodes[i].lo() != 0) out << i << " -> " << __nodes[i].lo();
    out << " }, ";

    out << "{ ";
    for (i = 2; i < __nodes.size() - 1; ++i)
      out << i << " -> " << __nodes[i].hi() << ", ";

    out << i << " -> " << __nodes[i].hi();
    out << " }, ";

    out << "{ ";
    for (i = 0; i < __nodes.size() - 1; ++i)
      if (__nodes[i].val() == node::value_max)
        // ⊥ ⊤ \u22a5\u22a5
        out << "LH"[i] << ", ";
      else
        out << (T)(__nodes[i].val()) << ", ";

    if (__nodes[i].val() == node::value_max)
      out << "LH"[i];
    else
      out << (T)(__nodes[i].val());

    out << " }";
    out << " }\n";

    return out;
  }

  friend ostream& operator<<(ostream& out, const zdd& z) {
    size_t i;
    out << "root => " << z.__root << endl;
    // out << "min => " << int( z.__nodes[z.__root].val() ) << endl;
    // out << "max => " << int( z.__max ) << endl;
    for (i = 0; i < z.__nodes.size() - 1; ++i)
      out << i << " => " << z.__nodes[i] << endl;
    out << i << " => " << z.__nodes[i];
    return out;
  }
};

#endif  // ZDD_HPP
