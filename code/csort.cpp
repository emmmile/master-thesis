#include <iostream>
#include <algorithm>
#include <vector>
#include <boost/timer.hpp>
using namespace boost;
using namespace std;

static const size_t trials = 10;
static const size_t n = 100000000;
static const size_t maxelement = 255;
typedef pair<size_t, size_t> entry;

bool order ( const entry& a, const entry& b ) {
    return a.first < b.first;
}

template<size_t MIN, size_t MAX, class T = size_t>
class keyget {
public:
    static const T min ( ) { return MIN; }
    static const T max ( ) { return MAX; }

    template<class Element>
    T operator() ( const Element& arg ) { return arg; }
};

template<class RandomIt, class Key, class Comp>
void csort ( RandomIt beg, RandomIt end, Key key, Comp comp ) {
    size_t keys = key.max() - key.min();
    size_t keylist [keys];
    size_t* aux = new size_t[distance( beg, end )]; // types?

    for ( size_t i = 0; i < keys; ++i ) keylist[i] = -1;

    for ( RandomIt i = beg; i != end; ++i ) {
        aux[distance( beg, i )] = keylist[key( *i )];
        keylist[key( *i )] = distance( beg, i );
    }

    typedef typename iterator_traits<RandomIt>::value_type keytype;

    keytype* out = new keytype[distance( beg, end )];
    keytype* current = out + distance( beg, end );
    for ( size_t i = keys - 1; i != -1; --i ) {
        for ( size_t j = keylist[i]; j != -1; ) {
            --current;
            *current = *(beg + j);

            j = aux[j];
        }
    }

    copy( out, out + distance( beg, end ), beg );
    delete[] out;
    delete[] aux;

    /*RandomIt current = end - 1;
    for ( size_t i = 0; i < keys; ++i ) { printf( "list[%d] -> %d\n" , i, keylist[i] ); }
    for ( size_t i = 0; i < distance( beg, end ); ++i ) { printf( "aux[%d] -> %d\n" , i, aux[i] ); }

    for ( size_t i = keys - 1; i != -1; --i ) {
        for ( size_t j = keylist[i]; j != -1; ) {
            printf( "(%d) j = %d ", i, j );

            if ( j > distance( beg, current ) ) {
                j = aux[j];
                printf( " skipping.\n" );
                continue;
            }

            printf( " swapping %d with %d.\n", distance( beg, current), j );
            swap( *current, *(beg + j) );



            size_t nextj = aux[j];
            aux[j] = keylist[key( *(beg + j) )];
            keylist[key( *(beg + j) )] = j;
            --current;
            j = nextj;

            for ( size_t i = 0; i < end - beg; ++i ) { cout << *(beg+i) << " "; } cout << endl;
            for ( size_t i = 0; i < keys; ++i ) { printf( "list[%d] -> %d\n" , i, keylist[i] ); }
            for ( size_t i = 0; i < distance( beg, end ); ++i ) { printf( "aux[%d] -> %d\n" , i, aux[i] ); }
            getchar();
        }

        while ( key(*current) == i ) {
            --current;
            printf( " skipping.\n" );
        }
    }*/
}

class lessthan {
public:
    size_t value;
    lessthan( size_t value ) : value( value ) { }

    template<class Element>
    bool operator() ( const Element& e ) {
        return e < value;
    }
};


int main() {
    typedef vector<size_t> vec;
    typedef vec::iterator vi;
    vec seq( n );

    for ( size_t i = 0; i < n; ++i ) { seq[i] = random() % maxelement; }
    timer elapsed;

    //vi bound = partition(seq.begin(), seq.end(), lessthan(maxelement / 2) );
    //sort( seq.begin(), bound );
    //sort( bound, seq.end() );
    sort(  seq.begin(), seq.end() );
    //csort( seq.begin(), seq.end(), keyget<0, maxelement>(), greater<size_t>() );

    //for ( size_t i = 0; i < n; ++i ) { cout << seq[i] << endl; }
    for ( size_t i = 1; i < n; ++i ) { if ( seq[i] < seq[i-1] ) exit( 1 ); }
    cout << "Sorted in " << elapsed.elapsed() << ".\n";



    //for ( size_t i = 0; i < n; ++i ) { cout << seq[i] << " "; }
    //cout << endl;

    // benchmark pairs vs pointers
    /*double avg = 0.0;

    for ( size_t j = 0; j < trials; ++j ) {
        // solution 1 pairs
        vector<entry> seq( n );
        for ( size_t i = 0; i < n; ++i ) { seq[i].second = i; seq[i].first = random() % n; }

        timer t;
        sort( seq.begin(), seq.end() );
        avg += t.elapsed();
        cout << t.elapsed() << endl;
    }

    cout << avg / trials << endl;
    avg = 0.0;

    for ( size_t j = 0; j < trials; ++j ) {
        // solution 1 pairs
        vector<size_t> seq( n );
        vector<size_t> inverse( n );
        for ( size_t i = 0; i < n; ++i ) { seq[i] = random() % n; }

        timer t;
        sort( seq.begin(), seq.end() );
        avg += t.elapsed();
        for ( size_t i = 0; i < n; ++i ) { inverse[seq[i]] = i; }
        cout << t.elapsed() << endl;
    }

    cout << avg / trials << endl;*/
    return 0;
}

