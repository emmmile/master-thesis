#include <iostream>
#include "bdds/sdd.hpp"
#include <boost/progress.hpp>
#include "bdds/sddbase.hpp"
#include <boost/program_options.hpp>
#include <fstream>
using namespace std;
using namespace boost;
using namespace boost::program_options;



int main( int argc, char** argv ) {
    string input = "data/X108771220.txt";
    size_t p = string::npos;


    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce this help message")
            ("input,i", value<string>(&input), "set the location of the input text")
            ("prefix,p", value<size_t>(&p), "set the lenght of the initial prefix")
            ;

    variables_map vm;

    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            exit( 1 );
        }

        notify(vm);
    } catch ( error& e ) {
        cerr << desc << endl;
        cerr << e.what() << endl;
        exit( 1 );
    }

    std::ifstream t(input);
    if ( !t ) {
        cerr << "file \"" << input << "\" not found.\n";
        exit( 1 );
    }

    std::string text(( istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
    text = text.substr( 0, p );
    text = trim( text );

    //cout << text << endl;

    //size_t memory = 0;
    //sddbase bseq;
    //node all = bseq.empty();

    for ( size_t j = 0; j <= text.size(); ++j ) {
        //cout << j << endl;
        cout << text.substr( j ) << endl;
    }

    /*printf( "SDD base size of %zu.\n", bseq.size() );
    printf( "SDD base max of size %zu.\n", memory );
    cout << "SDD union of size " << bseq.size( all ) << " (solutions: " << bseq.count( all ) << ").\n";*/


    return 0;
}

