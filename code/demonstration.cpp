#include <iostream>
#include "bdds/sdd.hpp"
#include <boost/progress.hpp>
#include "bdds/sddbase.hpp"
#include <boost/program_options.hpp>
#include <fstream>
#include <sys/resource.h>
using namespace std;
using namespace boost;
using namespace boost::program_options;



int main( int argc, char** argv ) {
    /*const rlim_t kStackSize = 32 * 1024 * 1024;   // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }*/


    string input = "data/X108771220.txt";
    size_t p = string::npos;

    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce this help message")
            ("input,i", value<string>(&input), "set the location of the input text")
            ("prefix,p", value<size_t>(&p), "set the lenght of the initial prefix")
            ;

    variables_map vm;

    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            exit( 1 );
        }

        notify(vm);
    } catch ( error& e ) {
        cerr << desc << endl;
        cerr << e.what() << endl;
        exit( 1 );
    }


    if ( vm.count("input") == 0 ) {
        cerr << "required option '--input' is missing.\n";
        exit( 1 );
    }

    std::ifstream t(input);
    if ( !t ) {
        cerr << "file \"" << input << "\" not found.\n";
        exit( 1 );
    }

    timer elapsed;
    std::string text(( istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
    text = trim( text );
    text = text.substr( 0, p );

    printf( "Read %zu symbols from '%s' in %lf s.\n", text.size(), input.c_str(), elapsed.elapsed() );

    sddbase base;
    node root = base.allsubstrings( text );
    elapsed.restart();
    root = base.computeallsubstrings();

    printf( "Finished in %lf s. ", elapsed.elapsed() ); cout.flush();
    cout << "There are exactly " << base.count( root ) << " different substrings.\n";
    cout << "Press ENTER to view a uniformly random substring: "; getchar();
    base.random( root );

    return 0;
}

