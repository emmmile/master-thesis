#ifndef ALPHABET_HPP
#define ALPHABET_HPP

#include <unordered_map>
#include <array>
#include <initializer_list>
#include <iostream>
#include <boost/bimap.hpp>
using namespace boost;
using namespace boost::bimaps;
using namespace std;

// basically a sorted array of symbols
template<class Symbol, size_t N>
class static_alphabet {
    array<Symbol, N> __values;
public:
    static_alphabet ( const initializer_list<Symbol>& v ) { //: __values( v ) {
        copy( v.begin(), v.end(), __values.begin() );
        sort( __values.begin(), __values.end() );
    }

    static constexpr size_t size ( ) {
        return N;
    }

    const size_t index ( Symbol s ) const {
        return find(__values.begin(), __values.end(), s) - __values.begin();
    }

    const Symbol symbol ( size_t i ) const {
        return __values[i];
    }

    const array<Symbol, N>& data ( ) const {
        return __values;
    }
};

//basically a map between symbols and integers
template<class Symbol>
class dynamic_alphabet {
    bimap<Symbol, size_t> __values;
    typedef typename bimap<Symbol, size_t>::value_type bpair;

    template<class C>
    void copy( C& v ) {
        size_t j = 0;
        for ( typename C::const_iterator i = v.cbegin(); i != v.cend(); ++i )
            __values.insert( bpair( *i, j++ ) );
    }

public:
    dynamic_alphabet ( const initializer_list<Symbol>& v ) {
        copy( v );
    }

    template<size_t N>
    dynamic_alphabet ( const static_alphabet<Symbol, N>& v ) {
        copy( v.data() );
    }

    const size_t index ( Symbol s ) const {
        return __values.left.count( s ) ? __values.left.at(s) : 0;
    }

    const Symbol symbol ( size_t i ) const {
        return __values.right.count( i ) ? __values.right.at(i) : 0;
    }

    const size_t size ( ) const {
        return __values.size();
    }
};


#endif // ALPHABET_HPP
