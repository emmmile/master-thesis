#ifndef STATIC_H
#define STATIC_H

#include <algorithm>
#include <string>
#include <cstring>
using namespace std;



/// This is a static implementation of std::string.
template<unsigned int N>
class staticstring {
    char __data[N + 1];

public:
    //  void write ( const char& c ) {
    //    __data[0] = c;
    //    __data[1] = '\0';
    //  }

    //  void write ( const char* s ) {
    //    strncpy( __data, s, N + 1 );
    //    if ( strlen(s) >= N + 1 ) __data[N] = '\0';
    //  }

    //  void write ( const string_array& s ) {
    //    write( s.str() );
    //  }

    //	void write ( const long int& i, const int width = N ) {
    //		itoa( i,__data, 10 );
    //		//snprintf( __data, N + 1, "%ld", i );
    //  }

    //	void write ( const int& i, const int width = N ) {
    //		itoa( i,__data, 10 );
    //		//snprintf( __data, N + 1, "%d", i );
    //  }

    //	void write ( float f, const int width = 4 ) {
    //    // sprintf does not support floating point number, neither width arguments like
    //    // ... "%*d", width, integer );
    //    // dtostrf solves partially the problem.
    //    // compute the decimal digits, that is width minus '.', sign and the integral part
    //    long decimaldigits = width - digits( f );
    //    // if f is too large it may not fit into width characters (even the integral part)
    //    // in this case cut the number to width bytes.
    //    if ( decimaldigits <= 0 ) {
    //			write( long( f ), width );
    //			//snprintf( __data, width + 1, "%ld", long( f ) );
    //      return;
    //    }

    //    //snprintf( __data, N + 1, "%.*f", decimaldigits, f );
    //    dtostrf( f, digits( f ), decimaldigits, __data );
    //  }

    //  string_array ( ) {
    //    clear();
    //  }

    //  string_array ( const char& c ) {
    //    write( c );
    //  }

    //  string_array ( const char* s ) {
    //    write( s );
    //  }

    //  string_array ( const long int& i ) {
    //    write( i );
    //  }

    //  string_array ( const int& i ) {
    //    write( i );
    //  }

    //  string_array ( float f, const unsigned long width = 4 ) {
    //    write( f, width );
    //  }

    //  string_array ( double d, const unsigned long width = 4 ) {
    //    write( d, width );
    //  }

    //  template<unsigned int M>
    //  stackstring ( const stackstring<M>& s ) {
    //    write( s.str() );
    //  }

    //  template<unsigned int M = N + 1>
    //  string_array<M> operator+ ( const char& c ) const {
    //    string_array<1> tmp( c );
    //    return *this + tmp;
    //  }

    //  template<class T> // here N must be large enough to hold the value v
    //  string_array operator+ ( const T& v ) const {
    //    string_array tmp( v );
    //    return *this + tmp;
    //  }

    //  template<unsigned int M>
    //  string_array<N+M> operator+ ( const string_array<M>& s ) const {
    //    string_array<N+M> out( *this );
    //    //printf( "'%s' '%s' => %d (%s)\n", __data, s.str(), N+M, out.str() );
    //    return out.append( s.str() );
    //  }

    //  string_array& append ( const char* s ) {
    //		strncpy(  __data + length(), s, N + 1 - length() );
    //    return *this;
    //  }

    //  string_array& append ( const string_array& s ) {
    //    return append( s.str() );
    //  }

    //  string_array& append ( long int i ) {
    //    string_array tmp( i );
    //    return append( tmp );
    //  }

    //  string_array& append ( float f, const unsigned long precision = 4 ) {
    //    string_array tmp( f, precision );
    //    return append( tmp );
    //  }
    const char& operator[] (size_t pos) const {
        return __data[pos];
    }

    char& operator[] (size_t pos) {
        return __data[pos];
    }

    void clear ( ) {
        memset( __data, '\0', N + 1 );
    }

    unsigned int length ( ) const {
        return strlen( __data );
    }

    const char* c_str () const {
        return __data;
    }

    const char* begin () const {
        return __data;
    }

    const char* end () const {
        return __data + N + 1;
    }

    char* begin () {
        return __data;
    }

    char* end () {
        return __data + N + 1;
    }

    bool operator!= ( const staticstring& another ) const {
        return !( *this == another);
    }

    bool operator== ( const staticstring& another ) const {
        return strcmp( __data, another.__data ) == 0;
    }

    bool operator< ( const staticstring& another ) const {
        return strcmp( __data, another.__data ) < 0;
    }





    staticstring& append (size_t n, char c) {
        __data[length() + n] = '\0';
        fill( __data + length(), __data + length() + n, c );
        return *this;
    }

    staticstring& erase ( size_t pos = 0, size_t len = N + 1 ) {
        char* newend = move( __data + pos + len, __data + N + 1, __data + pos );
        fill( newend, __data + N + 1, '\0' );
        return *this;
    }

    staticstring substr ( size_t pos = 0, size_t len = N + 1 ) const {
        staticstring out;
        copy( __data + pos, __data + pos + len, out.__data );
        return out;
    }

    void push_back ( char c ) {
        append( 1, c );
    }

    staticstring operator+ (const staticstring& r) const {
        staticstring out( *this );
        copy( r.__data, r.__data + r.length(), out.__data + length() );
        return out;
    }

    staticstring ( const std::string& s ) : staticstring() {
        copy( s.begin(), s.end(), __data );
    }

    staticstring ( const char* s ) : staticstring() {
        copy( s, s + strlen( s ), __data );
    }

    staticstring ( const char c ) : staticstring() {
        __data[0] = c;
    }

    staticstring ( ) {
        clear();
    }

    friend ostream& operator << ( ostream& out, const staticstring& s ) {
        return out << s.__data;
    }
};


namespace std {
template<unsigned int T>
class hash<staticstring<T> > {
public:
    // http://www.cse.yorku.ca/~oz/hash.html
    inline size_t operator() ( const staticstring<T>& s ) const {
        size_t hash = 5381;
        int c;
        const char* str = s.c_str();

        while ((c = *(str++)))
            hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

        return hash;
    }
};
}


#endif
