#ifndef DYNAMIC_STRING_HPP
#define DYNAMIC_STRING_HPP

#include <iostream>
#include <boost/dynamic_bitset.hpp>
#include "alphabet.hpp"
using namespace boost;
using namespace std;




/*
class dynamic_string {
  dynamic_bitset<> __data;

  inline size_t log2 ( size_t v ) {
    size_t out;
    for ( out = 0; v != 0; v >>= 1 ) ++out;
    return out;
  }

public:
  // encode s using alphabet a
  template<class Symbol>
  dynamic_string( const Symbol* s, size_t len, alphabet<Symbol>& a ) {
    size_t block = log2( a.size() - 1 );
    for ( size_t i = 0; i < len; ++i ) {
      size_t v = a[s[i]];
      for ( size_t j = 0; j < block; ++j )
        __data.push_back( v & (1<<j) );
    }
  }

  size_t size ( ) const {
    return __data.size();
  }

  size_t bytes ( ) const {
    return sizeof( __data );
  }

  friend ostream& operator << ( ostream& out, dynamic_string& s ) {
    return out << s.__data;
  }
};*/



#endif // DYNAMIC_STRING_HPP
