#ifndef TRIE_HPP
#define TRIE_HPP

#include <iostream>
#include <array>
using namespace std;




template<size_t S>
class trie_level {
  array<trie_level*, S> __pointers;
public:
  trie_level () {
    __pointers.fill( NULL );
  }

  trie_level* operator[] ( size_t s ) const {
    return __pointers[s];
  }

  void insert ( size_t s, trie_level* v ) {
    __pointers[s] = v;
  }
};

template<size_t S>
class trie {
  typedef trie_level<S> level;
  level __root;
  size_t __size;

  bool __insert_r ( const char* str, size_t i, level* l ) {
    if ( i == strlen( str ) ) return false;

    level* next = (*l)[str[i] - 'a'];
    if ( next != NULL ) {
      return __insert_r( str, i+1, next );
    } else {
      level* newlevel = new level;
      //l[i] = newlevel;
      l->insert(i, newlevel);
      __insert_r( str, i+1, newlevel );
      return true;
    }
  }

public:
  trie ( ) : __size( 0 ) {
  }

  ~trie ( ) {
  }

  bool insert ( const char* str ) {
    bool out = __insert_r( str, 0, &__root );
    if ( out ) ++__size;
    return out;
  }

  bool insert ( string& str ) {
    const char* cstr = str.c_str();
    return insert( cstr );
  }

  size_t size ( ) const {
    return __size;
  }
};

#endif // TRIE_HPP
