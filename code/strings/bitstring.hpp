#ifndef STATIC_STRING_HPP
#define STATIC_STRING_HPP


#include <iostream>
#include <bitset>
#include <boost/integer/static_log2.hpp>
#include "alphabet.hpp"
using namespace boost;
using namespace std;
using namespace math;


template<size_t N, size_t S, size_t LOGS = static_log2<S-1>::value + 1>
class bitstring {
public:
    bitset<N * LOGS> __bits;

    //  inline static const size_t log2 ( size_t v ) {
    //    size_t out;
    //    for ( out = 0; v != 0; v >>= 1 ) ++out;
    //    return out;
    //  }
    bitstring() {
    }

    inline void write_symbol( size_t v, size_t start ) {
        for ( size_t k = 0; k < LOGS; ++k )
            __bits[start++] = (v & (1<<k)) != 0;
    }

    inline size_t read_symbol( size_t start ) const {
        size_t index = 0;
        for ( size_t k = 0; k < LOGS; ++k )
            index |= __bits[start++] << k;
        return index;
    }

    template<class Symbol>
    static_string( const Symbol* str, static_alphabet<Symbol,S>& a ) {
        size_t j = 0;
        for ( size_t i = 0; i < N; ++i ) {
            size_t v = a.index( str[i] );
            for ( size_t k = 0; k < LOGS; ++k )
                //__bits[j++] = (v & (1<<k)) != 0;
                write_symbol( v, j );
            j += LOGS;
        }
    }

    template<class Symbol>
    Symbol* decode ( Symbol* str, static_alphabet<Symbol,S>& a ) const {
        size_t j = 0;
        for ( size_t i = 0; i < N; ++i ) {
            size_t index = read_symbol( j );
            j += LOGS;

            str[i] = a.symbol( index );
        }

        return str;
    }

    const size_t operator[] ( size_t j ) const {
        return read_symbol( j*LOGS );
    }

    constexpr size_t bytes ( ) const {
        return sizeof( __bits );
    }

    constexpr size_t size ( ) const {
        return N;
    }

    const unsigned long to_ulong ( ) const {
        return __bits.to_ulong();
    }

    friend ostream& operator << ( ostream& out, bitstring& s ) {
        return out << s.__bits;
    }

    inline bool operator== ( const bitstring& s ) const {
        return s.__bits == __bits;
    }

    bitstring& operator= ( const bitstring& s ) {
        __bits = s.__bits;
        return *this;
    }

    // editing
    bitstring<N-1, S> remove ( const size_t j ) const {
        bitstring<N-1, S> out;
        for ( size_t i = 0; i < j * LOGS; ++i )
            out.__bits[i] = __bits[i];

        for ( size_t i = (j+1) * LOGS; i < N * LOGS; ++i )
            out.__bits[i-LOGS] = __bits[i];

        return out;
    }

    bitstring<N+1, S> insert ( const size_t j, const size_t& v ) const {
        bitstring<N+1, S> out;
        for ( size_t i = 0; i < j * LOGS; ++i )
            out.__bits[i] = __bits[i];

        out.write_symbol( v, j * LOGS );

        for ( size_t i = (j+1) * LOGS; i < N * LOGS; ++i )
            out.__bits[i+LOGS] = __bits[i];

        return out;
    }

    template<class Symbol>
    bitstring<N+1, S> insert ( const size_t j, const Symbol& s, const static_alphabet<Symbol,S>& a ) const {
        return insert( j, a.index( s ) );
    }

    bitstring<N+1, S> append ( const size_t& v ) const {
        return insert( N, v );
    }

    template<class Symbol>
    bitstring<N+1, S> append ( const Symbol& s, const static_alphabet<Symbol,S>& a ) const {
        return insert( N, a.index( s ) );
    }

    // bitstring& shuffle ( Random& gen ) {
    //     for ( size_t i = 0; i < N; ++i ) {
    //         size_t v = gen.integer() % S;
    //         write_symbol( v, i * LOGS );
    //     }

    //     return *this;
    // }

    template<size_t L>
    bitstring<L, S> substring ( const size_t start ) const {
        bitstring<L, S> out;
        for ( size_t i = start * LOGS; i < (start+L) * LOGS; ++i )
            out.__bits[i] = __bits[i];
        return out;
    }
};


#endif // STATIC_STRING_HPP
