#include <iostream>
#include <algorithm>
#include "strings/static.hpp"
#include <cassert>
#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include "bdds/sdd.hpp"
#include "bdds/sddbase.hpp"
#include "subsequences.h"
#include <boost/program_options.hpp>
#include <boost/progress.hpp>
using namespace std;
using namespace boost;
using namespace boost::program_options;






int main( int argc, char** argv ) {
    string input = "input";
    size_t p = string::npos;


    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce this help message")
            ("input,i", value<string>(&input), "set the location of the input text")
            ("prefix,p", value<size_t>(&p), "set the lenght of the initial prefix")
            ;

    variables_map vm;

    try {
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << "\n";
            exit( 1 );
        }

        notify(vm);
    } catch ( error& e ) {
        cerr << desc << endl;
        cerr << e.what() << endl;
        exit( 1 );
    }


    if ( !vm.count("input") ) {
        cerr << "option required.\n";
        exit( 1 );
    }

    std::ifstream t(input);
    if ( !t ) {
        cerr << "file \"" << input << "\" not found.\n";
        exit( 1 );
    }


    std::string text(( istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
    text = text.substr( 0, p );
    text = trim( text );


    size_t n = text.size();
    //cout << n << " " << (1 << n) << endl; getchar();
    for ( size_t conf = 0; conf < (1u << n); ++conf ) {
        bitset<64> bits( conf );
        for ( size_t i = 0; i < n; ++i ) {
            if ( bits[i] ) cout << text[i];
        }

        cout << endl;
    }


    return 0;
}

